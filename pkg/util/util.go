package util

import (
	_ "embed"
	"encoding/json"
	"errors"
	"fmt"
	"hypixel-bot/pkg/bot"
	"regexp"
	"strconv"
	"time"

	"github.com/valyala/fasthttp"

	"github.com/SevereCloud/vksdk/v2/api/params"
)

//go:embed font.ttf
var Font []byte
var Client = fasthttp.Client{ReadTimeout: time.Second * 10}
var Cache map[string]Mojang

var NameRegex = regexp.MustCompile("^[a-zA-Z0-9_]{3,16}$")

func FormatInt[T int | int32 | int64 | float32 | float64](n T) string {
	i := int64(n)

	in := strconv.FormatInt(i, 10)
	numOfDigits := len(in)
	if n < 0 {
		numOfDigits-- // First character is the - sign (not a digit)
	}
	numOfCommas := (numOfDigits - 1) / 3

	out := make([]byte, len(in)+numOfCommas)
	if n < 0 {
		in, out[0] = in[1:], '-'
	}

	for i, j, k := len(in)-1, len(out)-1, 0; ; i, j = i-1, j-1 {
		out[j] = in[i]
		if i == 0 {
			return string(out)
		}
		if k++; k == 3 {
			j, k = j-1, 0
			out[j] = ','
		}
	}
}

func GetHypixelApi(method string, args string) (response []byte, err error) {
	code, res, err := Client.Get(nil, "https://api.hypixel.net/"+method+"?key="+bot.HypixelKey+args)
	if err != nil {
		return
	}
	switch code {
	case 200:
		return res, err
	case 403:
		return nil, errors.New("Invalid API key")
	case 429:
		return nil, errors.New("Key throttle")
	}
	return
}

func GetRanked(uuid string) (response *RankedResponse, err error) {
	data, err := GetHypixelApi("player/ranked/skywars", "&uuid="+uuid)
	if err != nil {
		return
	}

	fmt.Println(string(data))

	err = json.Unmarshal([]byte(data), &response)
	if err != nil {
		return
	}

	return
}

func GetUUID(name string) (mojang *Mojang, err error) {
	if mojang, ok := Cache[name]; ok {
		return &mojang, nil
	}
	code, res, err := Client.Get(nil, "https://api.mojang.com/users/profiles/minecraft/"+name)
	if err != nil {
		return
	}
	if code != 200 {
		return nil, errors.New("Несуществующий ник")
	}

	mojang = &Mojang{}
	err = json.Unmarshal(res, &mojang)
	if err != nil {
		return
	}
	Cache[name] = *mojang
	return
}

func GetName(uuid string) (name string, err error) {
	if uuid == "" {
		return "what", errors.New("waht the fuck") // это не должно происходить никогда вообще бялть
	}

	code, res, err := Client.Get(nil, "https://api.mojang.com/user/profiles/"+uuid+"/names")
	if err != nil {
		return
	}
	if code != 200 {
		return "", errors.New("не удалось получить ник")
	}

	n := []Name{}
	err = json.Unmarshal(res, &n)
	if err != nil {
		return
	}

	return n[len(n)-1].Name, nil
}

func GetPlayer(name string) (response Player, err error) {
	uuid, err := GetUUID(name)
	if err != nil {
		return
	}
	res, err := GetHypixelApi("player", "&uuid="+uuid.Id)
	if err != nil {
		return
	}
	player := &PlayerResponse{}
	err = json.Unmarshal(res, &player)
	if err != nil {
		return
	}
	return player.Player, err
}

func SendMessage(peer_id int, message string) (err error) {
	msg := params.NewMessagesSendBuilder()
	msg.Message(message)
	msg.RandomID(0)
	msg.PeerID(peer_id)

	_, err = bot.VK.MessagesSend(msg.Params)
	if err != nil {
		return
	}
	return
}

func GetDBName(id int) (name string, err error) {
	err = bot.DB.QueryRow("SELECT name FROM users WHERE id = ?", strconv.Itoa(id)).Scan(&name)
	if err != nil {
		return
	}

	if name == "" || name == "false" {
		return "", errors.New("У вас не установлен ник.\nДля установки ника пропишите \"/name ник\"")
	}

	return
}
