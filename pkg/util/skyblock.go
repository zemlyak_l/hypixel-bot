package util

type HypixelSkyblockReturn struct {
	Success  bool `json:"success"`
	Profiles []SkyblockProfile
}

type SkyblockProfile struct {
	ProfileID string                    `json:"profile_id"`
	Members   map[string]SkyblockMember `json:"members"`
	Banking   Banking                   `json:"banking"`
}

type Banking struct {
	Balance      float64 `json:"balance"`
	Transactions []struct {
		Amount        float64 `json:"amount"`
		Timestamp     float64 `json:"timestamp"`
		Action        string  `json:"action"`
		InitiatorName string  `json:"initiator_name"`
	}
}

type SkyblockMember struct {
	LastSave int64   `json:"last_save"`
	Banking  Banking `json:"banking"`
	InvArmor struct {
		Type float64 `json:"type"`
		Data string  `json:"data"`
	} `json:"inv_armor"`
	FirstJoin    float64 `json:"first_join"`
	FirstJoinHub float64 `json:"first_join_hub"`
	Stats        struct {
		Deaths                                              float64 `json:"deaths"`
		DeathsEmeraldSlime                                  float64 `json:"deaths_emerald_slime"`
		HighestCritDamage                                   float64 `json:"highest_crit_damage"`
		Kills                                               float64 `json:"kills"`
		KillsEmeraldSlime                                   float64 `json:"kills_emerald_slime"`
		DeathsDiamondZombie                                 float64 `json:"deaths_diamond_zombie"`
		DeathsDiamondSkeleton                               float64 `json:"deaths_diamond_skeleton"`
		DeathsRedstonePigman                                float64 `json:"deaths_redstone_pigman"`
		KillsPig                                            float64 `json:"kills_pig"`
		KillsChicken                                        float64 `json:"kills_chicken"`
		KillsCow                                            float64 `json:"kills_cow"`
		KillsZombie                                         float64 `json:"kills_zombie"`
		KillsSkeleton                                       float64 `json:"kills_skeleton"`
		KillsZombieVillager                                 float64 `json:"kills_zombie_villager"`
		DeathsUnburriedZombie                               float64 `json:"deaths_unburried_zombie"`
		DeathsSplitterSpider                                float64 `json:"deaths_splitter_spider"`
		KillsSplitterSpider                                 float64 `json:"kills_splitter_spider"`
		KillsSplitterSpiderSilverfish                       float64 `json:"kills_splitter_spider_silverfish"`
		DeathsRandomSlime                                   float64 `json:"deaths_random_slime"`
		KillsRandomSlime                                    float64 `json:"kills_random_slime"`
		KillsRespawningSkeleton                             float64 `json:"kills_respawning_skeleton"`
		KillsJockeyShotSilverfish                           float64 `json:"kills_jockey_shot_silverfish"`
		KillsDasherSpider                                   float64 `json:"kills_dasher_spider"`
		DeathsZombieVillager                                float64 `json:"deaths_zombie_villager"`
		KillsUnburriedZombie                                float64 `json:"kills_unburried_zombie"`
		KillsRabbit                                         float64 `json:"kills_rabbit"`
		KillsSheep                                          float64 `json:"kills_sheep"`
		KillsSpider                                         float64 `json:"kills_spider"`
		KillsEnderman                                       float64 `json:"kills_enderman"`
		KillsWitch                                          float64 `json:"kills_witch"`
		DeathsVoid                                          float64 `json:"deaths_void"`
		DeathsFire                                          float64 `json:"deaths_fire"`
		KillsRuinWolf                                       float64 `json:"kills_ruin_wolf"`
		DeathsOldWolf                                       float64 `json:"deaths_old_wolf"`
		ItemsFished                                         float64 `json:"items_fished"`
		ItemsFishedNormal                                   float64 `json:"items_fished_normal"`
		KillsPondSquid                                      float64 `json:"kills_pond_squid"`
		DeathsSeaWalker                                     float64 `json:"deaths_sea_walker"`
		ItemsFishedTreasure                                 float64 `json:"items_fished_treasure"`
		DeathsSeaArcher                                     float64 `json:"deaths_sea_archer"`
		KillsSeaArcher                                      float64 `json:"kills_sea_archer"`
		KillsSeaWalker                                      float64 `json:"kills_sea_walker"`
		DeathsWitherSkeleton                                float64 `json:"deaths_wither_skeleton"`
		KillsWitherSkeleton                                 float64 `json:"kills_wither_skeleton"`
		KillsMagmaCube                                      float64 `json:"kills_magma_cube"`
		KillsBlaze                                          float64 `json:"kills_blaze"`
		DeathsFireballMagmaCube                             float64 `json:"deaths_fireball_magma_cube"`
		KillsLapisZombie                                    float64 `json:"kills_lapis_zombie"`
		KillsRedstonePigman                                 float64 `json:"kills_redstone_pigman"`
		KillsDiamondZombie                                  float64 `json:"kills_diamond_zombie"`
		AuctionsBids                                        float64 `json:"auctions_bids"`
		AuctionsHighestBid                                  float64 `json:"auctions_highest_bid"`
		KillsDiamondSkeleton                                float64 `json:"kills_diamond_skeleton"`
		DeathsLapisZombie                                   float64 `json:"deaths_lapis_zombie"`
		AuctionsWon                                         float64 `json:"auctions_won"`
		AuctionsBoughtRare                                  float64 `json:"auctions_bought_rare"`
		AuctionsGoldSpent                                   float64 `json:"auctions_gold_spent"`
		AuctionsBoughtUncommon                              float64 `json:"auctions_bought_uncommon"`
		DeathsRuinWolf                                      float64 `json:"deaths_ruin_wolf"`
		DeathsVoraciousSpider                               float64 `json:"deaths_voracious_spider"`
		KillsWeaverSpider                                   float64 `json:"kills_weaver_spider"`
		DeathsBlaze                                         float64 `json:"deaths_blaze"`
		KillsPigman                                         float64 `json:"kills_pigman"`
		DeathsPigman                                        float64 `json:"deaths_pigman"`
		KillsFireballMagmaCube                              float64 `json:"kills_fireball_magma_cube"`
		DeathsEnderman                                      float64 `json:"deaths_enderman"`
		DeathsZombie                                        float64 `json:"deaths_zombie"`
		DeathsSkeleton                                      float64 `json:"deaths_skeleton"`
		AuctionsBoughtCommon                                float64 `json:"auctions_bought_common"`
		KillsInvisibleCreeper                               float64 `json:"kills_invisible_creeper"`
		KillsSpiderJockey                                   float64 `json:"kills_spider_jockey"`
		KillsJockeySkeleton                                 float64 `json:"kills_jockey_skeleton"`
		KillsVoraciousSpider                                float64 `json:"kills_voracious_spider"`
		DeathsDasherSpider                                  float64 `json:"deaths_dasher_spider"`
		DeathsSpiderJockey                                  float64 `json:"deaths_spider_jockey"`
		DeathsSplitterSpiderSilverfish                      float64 `json:"deaths_splitter_spider_silverfish"`
		DeathsFall                                          float64 `json:"deaths_fall"`
		EndRaceBestTime                                     float64 `json:"end_race_best_time"`
		DeathsHowlingSpirit                                 float64 `json:"deaths_howling_spirit"`
		KillsSeaGuardian                                    float64 `json:"kills_sea_guardian"`
		AuctionsCreated                                     float64 `json:"auctions_created"`
		AuctionsFees                                        float64 `json:"auctions_fees"`
		AuctionsNoBids                                      float64 `json:"auctions_no_bids"`
		AuctionsCompleted                                   float64 `json:"auctions_completed"`
		AuctionsSoldCommon                                  float64 `json:"auctions_sold_common"`
		AuctionsGoldEarned                                  float64 `json:"auctions_gold_earned"`
		KillsGhast                                          float64 `json:"kills_ghast"`
		DeathsSpider                                        float64 `json:"deaths_spider"`
		DeathsEndermite                                     float64 `json:"deaths_endermite"`
		KillsEndermite                                      float64 `json:"kills_endermite"`
		KillsWatcher                                        float64 `json:"kills_watcher"`
		DeathsObsidianWither                                float64 `json:"deaths_obsidian_wither"`
		DeathsZealotEnderman                                float64 `json:"deaths_zealot_enderman"`
		DeathsWatcher                                       float64 `json:"deaths_watcher"`
		HighestCriticalDamage                               float64 `json:"highest_critical_damage"`
		GiftsReceived                                       float64 `json:"gifts_received"`
		GiftsGiven                                          float64 `json:"gifts_given"`
		KillsObsidianWither                                 float64 `json:"kills_obsidian_wither"`
		KillsLiquidHotMagma                                 float64 `json:"kills_liquid_hot_magma"`
		DeathsLiquidHotMagma                                float64 `json:"deaths_liquid_hot_magma"`
		MostWfloat64erSnowballsHit                          float64 `json:"most_wfloat64er_snowballs_hit"`
		MostWfloat64erDamageDealt                           float64 `json:"most_wfloat64er_damage_dealt"`
		MostWfloat64erMagmaDamageDealt                      float64 `json:"most_wfloat64er_magma_damage_dealt"`
		DeathsMagmaCube                                     float64 `json:"deaths_magma_cube"`
		AuctionsBoughtEpic                                  float64 `json:"auctions_bought_epic"`
		ItemsFishedLargeTreasure                            float64 `json:"items_fished_large_treasure"`
		AuctionsSoldUncommon                                float64 `json:"auctions_sold_uncommon"`
		KillsBatPinata                                      float64 `json:"kills_bat_pinata"`
		KillsOldWolf                                        float64 `json:"kills_old_wolf"`
		ForagingRaceBestTime                                float64 `json:"foraging_race_best_time"`
		DeathsWolf                                          float64 `json:"deaths_wolf"`
		KillsZealotEnderman                                 float64 `json:"kills_zealot_enderman"`
		KillsFrozenSteve                                    float64 `json:"kills_frozen_steve"`
		AuctionsSoldRare                                    float64 `json:"auctions_sold_rare"`
		KillsCreeper                                        float64 `json:"kills_creeper"`
		KillsGeneratorMagmaCube                             float64 `json:"kills_generator_magma_cube"`
		KillsHorsemanZombie                                 float64 `json:"kills_horseman_zombie"`
		KillsProtectorDragon                                float64 `json:"kills_protector_dragon"`
		KillsHorsemanBat                                    float64 `json:"kills_horseman_bat"`
		KillsNightSquid                                     float64 `json:"kills_night_squid"`
		EnderCrystalsDestroyed                              float64 `json:"ender_crystals_destroyed"`
		KillsGeneratorGhast                                 float64 `json:"kills_generator_ghast"`
		KillsChickenDeep                                    float64 `json:"kills_chicken_deep"`
		KillsZombieDeep                                     float64 `json:"kills_zombie_deep"`
		KillsCatfish                                        float64 `json:"kills_catfish"`
		KillsSeaLeech                                       float64 `json:"kills_sea_leech"`
		AuctionsSoldEpic                                    float64 `json:"auctions_sold_epic"`
		DeathsOldDragon                                     float64 `json:"deaths_old_dragon"`
		KillsBroodMotherSpider                              float64 `json:"kills_brood_mother_spider"`
		KillsBroodMotherCaveSpider                          float64 `json:"kills_brood_mother_cave_spider"`
		DeathsUnknown                                       float64 `json:"deaths_unknown"`
		KillsGuardianDefender                               float64 `json:"kills_guardian_defender"`
		KillsSlime                                          float64 `json:"kills_slime"`
		KillsPlayer                                         float64 `json:"kills_player"`
		DeathsPlayer                                        float64 `json:"deaths_player"`
		DeathsYoungDragon                                   float64 `json:"deaths_young_dragon"`
		KillsMagmaCubeBoss                                  float64 `json:"kills_magma_cube_boss"`
		KillsFrostyTheSnowman                               float64 `json:"kills_frosty_the_snowman"`
		KillsGeneratorSlime                                 float64 `json:"kills_generator_slime"`
		KillsDeepSeaProtector                               float64 `json:"kills_deep_sea_protector"`
		KillsWaterHydra                                     float64 `json:"kills_water_hydra"`
		KillsCarrotKing                                     float64 `json:"kills_carrot_king"`
		DeathsUnstableDragon                                float64 `json:"deaths_unstable_dragon"`
		AuctionsBoughtLegendary                             float64 `json:"auctions_bought_legendary"`
		KillsUnstableDragon                                 float64 `json:"kills_unstable_dragon"`
		KillsHeadlessHorseman                               float64 `json:"kills_headless_horseman"`
		DeathsCorruptedProtector                            float64 `json:"deaths_corrupted_protector"`
		PetMilestoneSeaCreaturesKilled                      float64 `json:"pet_milestone_sea_creatures_killed"`
		KillsSkeletonEmperor                                float64 `json:"kills_skeleton_emperor"`
		DeathsDrowning                                      float64 `json:"deaths_drowning"`
		ShredderBait                                        float64 `json:"shredder_bait"`
		ShredderFished                                      float64 `json:"shredder_fished"`
		KillsSeaWitch                                       float64 `json:"kills_sea_witch"`
		PetMilestoneOresMined                               float64 `json:"pet_milestone_ores_mined"`
		KillsPackSpirit                                     float64 `json:"kills_pack_spirit"`
		KillsSoulOfTheAlpha                                 float64 `json:"kills_soul_of_the_alpha"`
		KillsHowlingSpirit                                  float64 `json:"kills_howling_spirit"`
		KillsGuardianEmperor                                float64 `json:"kills_guardian_emperor"`
		KillsTarantulaSpider                                float64 `json:"kills_tarantula_spider"`
		DeathsTarantulaSpider                               float64 `json:"deaths_tarantula_spider"`
		KillsNightRespawningSkeleton                        float64 `json:"kills_night_respawning_skeleton"`
		KillsHorsemanHorse                                  float64 `json:"kills_horseman_horse"`
		KillsCorruptedProtector                             float64 `json:"kills_corrupted_protector"`
		AuctionsBoughtSpecial                               float64 `json:"auctions_bought_special"`
		AuctionsSoldLegendary                               float64 `json:"auctions_sold_legendary"`
		DungeonHubCrystalCoreNothingNoReturnBestTime        float64 `json:"dungeon_hub_crystal_core_nothing_no_return_best_time"`
		DungeonHubGiantMushroomNoAbilitiesNoReturnBestTime  float64 `json:"dungeon_hub_giant_mushroom_no_abilities_no_return_best_time"`
		DungeonHubPrecursorRuinsNoAbilitiesNoReturnBestTime float64 `json:"dungeon_hub_precursor_ruins_no_abilities_no_return_best_time"`
		KillsCryptLurker                                    float64 `json:"kills_crypt_lurker"`
		DeathsLostAdventurer                                float64 `json:"deaths_lost_adventurer"`
		KillsSniperSkeleton                                 float64 `json:"kills_sniper_skeleton"`
		DeathsTrap                                          float64 `json:"deaths_trap"`
		KillsCryptTankZombie                                float64 `json:"kills_crypt_tank_zombie"`
		KillsSkeletonGrunt                                  float64 `json:"kills_skeleton_grunt"`
		KillsDungeonRespawningSkeleton                      float64 `json:"kills_dungeon_respawning_skeleton"`
		KillsScaredSkeleton                                 float64 `json:"kills_scared_skeleton"`
		KillsCryptDreadlord                                 float64 `json:"kills_crypt_dreadlord"`
		KillsZombieGrunt                                    float64 `json:"kills_zombie_grunt"`
		KillsCryptSouleater                                 float64 `json:"kills_crypt_souleater"`
		KillsSkeletonSoldier                                float64 `json:"kills_skeleton_soldier"`
		KillsCryptUndead                                    float64 `json:"kills_crypt_undead"`
		KillsWatcherSummonUndead                            float64 `json:"kills_watcher_summon_undead"`
		KillsBonzoSummonUndead                              float64 `json:"kills_bonzo_summon_undead"`
		KillsCellarSpider                                   float64 `json:"kills_cellar_spider"`
		KillsLostAdventurer                                 float64 `json:"kills_lost_adventurer"`
		KillsSkeletonMaster                                 float64 `json:"kills_skeleton_master"`
		KillsDiamondGuy                                     float64 `json:"kills_diamond_guy"`
		DeathsSkeletonMaster                                float64 `json:"deaths_skeleton_master"`
		KillsParasite                                       float64 `json:"kills_parasite"`
		KillsScarfWarrior                                   float64 `json:"kills_scarf_warrior"`
		DeathsSkeletor                                      float64 `json:"deaths_skeletor"`
		KillsZombieSoldier                                  float64 `json:"kills_zombie_soldier"`
		KillsProfessorGuardianSummon                        float64 `json:"kills_professor_guardian_summon"`
		KillsBlazeHigherOrLower                             float64 `json:"kills_blaze_higher_or_lower"`
		DeathsScarfWarrior                                  float64 `json:"deaths_scarf_warrior"`
		DeathsSkeletonSoldier                               float64 `json:"deaths_skeleton_soldier"`
		DeathsBonzo                                         float64 `json:"deaths_bonzo"`
		DeathsWatcherSummonUndead                           float64 `json:"deaths_watcher_summon_undead"`
		KillsDungeonSecretBat                               float64 `json:"kills_dungeon_secret_bat"`
		DeathsCryptLurker                                   float64 `json:"deaths_crypt_lurker"`
		DeathsDungeonRespawningSkeleton                     float64 `json:"deaths_dungeon_respawning_skeleton"`
		KillsCryptUndeadAlexander                           float64 `json:"kills_crypt_undead_alexander"`
		KillsCryptUndeadMarius                              float64 `json:"kills_crypt_undead_marius"`
		DeathsCryptDreadlord                                float64 `json:"deaths_crypt_dreadlord"`
		KillsLonelySpider                                   float64 `json:"kills_lonely_spider"`
		KillsScarfArcher                                    float64 `json:"kills_scarf_archer"`
		KillsScarfPriest                                    float64 `json:"kills_scarf_priest"`
		DeathsScarf                                         float64 `json:"deaths_scarf"`
		KillsScarfMage                                      float64 `json:"kills_scarf_mage"`
		KillsCryptUndeadCodenameB                           float64 `json:"kills_crypt_undead_codename_b"`
		KillsCryptUndeadRelenter                            float64 `json:"kills_crypt_undead_relenter"`
		KillsCryptUndeadSfarnham                            float64 `json:"kills_crypt_undead_sfarnham"`
		KillsCryptUndeadNitroholic                          float64 `json:"kills_crypt_undead_nitroholic_"`
		KillsCryptUndeadLikaos                              float64 `json:"kills_crypt_undead_likaos"`
		KillsCryptUndeadConnorlinfoot                       float64 `json:"kills_crypt_undead_connorlinfoot"`
		KillsCryptUndeadDctr                                float64 `json:"kills_crypt_undead_dctr"`
		KillsCryptUndeadJayavarmen                          float64 `json:"kills_crypt_undead_jayavarmen"`
		KillsCryptUndeadApunch                              float64 `json:"kills_crypt_undead_apunch"`
		KillsCryptUndeadMinikloon                           float64 `json:"kills_crypt_undead_minikloon"`
		KillsCryptUndeadThorlon                             float64 `json:"kills_crypt_undead_thorlon"`
		KillsCryptUndeadAgentk                              float64 `json:"kills_crypt_undead_agentk"`
		KillsCryptUndeadRobitybobity                        float64 `json:"kills_crypt_undead_robitybobity"`
		KillsCryptUndeadBembo                               float64 `json:"kills_crypt_undead_bembo"`
		KillsCryptUndeadLuckykessie                         float64 `json:"kills_crypt_undead_luckykessie"`
		KillsCryptUndeadWilliamtiger                        float64 `json:"kills_crypt_undead_williamtiger"`
		KillsCryptUndeadSkyerzz                             float64 `json:"kills_crypt_undead_skyerzz"`
		KillsCryptUndeadRevengeee                           float64 `json:"kills_crypt_undead_revengeee"`
		KillsCryptUndeadJudg3                               float64 `json:"kills_crypt_undead_judg3"`
		KillsCryptUndeadErosemberg                          float64 `json:"kills_crypt_undead_erosemberg"`
		KillsCryptUndeadRezzus                              float64 `json:"kills_crypt_undead_rezzus"`
		KillsCryptUndeadBloozing                            float64 `json:"kills_crypt_undead_bloozing"`
		KillsCryptUndeadHypixel                             float64 `json:"kills_crypt_undead_hypixel"`
		KillsCryptUndeadDonpireso                           float64 `json:"kills_crypt_undead_donpireso"`
		KillsCryptUndeadPjoke1                              float64 `json:"kills_crypt_undead_pjoke1"`
		KillsCryptUndeadCecer                               float64 `json:"kills_crypt_undead_cecer"`
		KillsCryptUndeadFlameboy101                         float64 `json:"kills_crypt_undead_flameboy101"`
		KillsCryptUndeadChilynn                             float64 `json:"kills_crypt_undead_chilynn"`
		KillsCryptUndeadPlancke                             float64 `json:"kills_crypt_undead_plancke"`
		KillsCryptUndeadSylent                              float64 `json:"kills_crypt_undead_sylent"`
		KillsCryptUndeadExternalizable                      float64 `json:"kills_crypt_undead_externalizable"`
		KillsCryptUndeadOrangemarshall                      float64 `json:"kills_crypt_undead_orangemarshall"`
		KillsCryptUndeadJamiethegeek                        float64 `json:"kills_crypt_undead_jamiethegeek"`
		KillsCryptUndeadLadybleu                            float64 `json:"kills_crypt_undead_ladybleu"`
		KillsCryptUndeadValentin                            float64 `json:"kills_crypt_undead_valentin"`
		KillsCryptUndeadFriedrich                           float64 `json:"kills_crypt_undead_friedrich"`
		KillsCryptUndeadBernhard                            float64 `json:"kills_crypt_undead_bernhard"`
		KillsDungeonRespawningSkeletonSkull                 float64 `json:"kills_dungeon_respawning_skeleton_skull"`
		DeathsDiamondGuy                                    float64 `json:"deaths_diamond_guy"`
		KillsCryptUndeadChristian                           float64 `json:"kills_crypt_undead_christian"`
		KillsCryptUndeadNicholas                            float64 `json:"kills_crypt_undead_nicholas"`
		DeathsCryptUndead                                   float64 `json:"deaths_crypt_undead"`
		KillsCryptUndeadPieter                              float64 `json:"kills_crypt_undead_pieter"`
		KillsSkeletor                                       float64 `json:"kills_skeletor"`
		DeathsProfessorMageGuardian                         float64 `json:"deaths_professor_mage_guardian"`
		KillsOldDragon                                      float64 `json:"kills_old_dragon"`
		KillsShadowAssassin                                 float64 `json:"kills_shadow_assassin"`
		DeathsShadowAssassin                                float64 `json:"deaths_shadow_assassin"`
		KillsZombieKnight                                   float64 `json:"kills_zombie_knight"`
		DeathsZombieKnight                                  float64 `json:"deaths_zombie_knight"`
		DeathsDeathmite                                     float64 `json:"deaths_deathmite"`
		DeathsCryptSouleater                                float64 `json:"deaths_crypt_souleater"`
		KillsDeathmite                                      float64 `json:"kills_deathmite"`
		KillsWatcherBonzo                                   float64 `json:"kills_watcher_bonzo"`
		DeathsProfessorGuardianSummon                       float64 `json:"deaths_professor_guardian_summon"`
		KillsSuperTankZombie                                float64 `json:"kills_super_tank_zombie"`
		KillsSpiritSheep                                    float64 `json:"kills_spirit_sheep"`
		KillsSpiritBull                                     float64 `json:"kills_spirit_bull"`
		KillsSpiritWolf                                     float64 `json:"kills_spirit_wolf"`
		KillsSpiritRabbit                                   float64 `json:"kills_spirit_rabbit"`
		DeathsSpiritBull                                    float64 `json:"deaths_spirit_bull"`
		KillsSuperArcher                                    float64 `json:"kills_super_archer"`
		DeathsSpiritChicken                                 float64 `json:"deaths_spirit_chicken"`
		DeathsSpiritMiniboss                                float64 `json:"deaths_spirit_miniboss"`
		KillsSpiritChicken                                  float64 `json:"kills_spirit_chicken"`
		DeathsSpiritBat                                     float64 `json:"deaths_spirit_bat"`
		KillsCryptWitherskeleton                            float64 `json:"kills_crypt_witherskeleton"`
		KillsSpiritMiniboss                                 float64 `json:"kills_spirit_miniboss"`
		DeathsSpiritRabbit                                  float64 `json:"deaths_spirit_rabbit"`
		KillsWatcherScarf                                   float64 `json:"kills_watcher_scarf"`
		KillsSpiritBat                                      float64 `json:"kills_spirit_bat"`
		KillsThorn                                          float64 `json:"kills_thorn"`
		KillsKingMidas                                      float64 `json:"kills_king_midas"`
		DeathsWiseDragon                                    float64 `json:"deaths_wise_dragon"`
		KillsRevenantZombie                                 float64 `json:"kills_revenant_zombie"`
		DeathsZombieSoldier                                 float64 `json:"deaths_zombie_soldier"`
		MythosBurrowsDugNext                                float64 `json:"mythos_burrows_dug_next"`
		MythosBurrowsDugNextNull                            float64 `json:"mythos_burrows_dug_next_null"`
		MythosBurrowsDugCombat                              float64 `json:"mythos_burrows_dug_combat"`
		MythosBurrowsDugCombatNull                          float64 `json:"mythos_burrows_dug_combat_null"`
		MythosKills                                         float64 `json:"mythos_kills"`
		KillsMinosHunter                                    float64 `json:"kills_minos_hunter"`
		MythosBurrowsDugTreasure                            float64 `json:"mythos_burrows_dug_treasure"`
		MythosBurrowsDugTreasureNull                        float64 `json:"mythos_burrows_dug_treasure_null"`
		MythosBurrowsChainsComplete                         float64 `json:"mythos_burrows_chains_complete"`
		MythosBurrowsChainsCompleteNull                     float64 `json:"mythos_burrows_chains_complete_null"`
		MythosBurrowsDugNextCOMMON                          float64 `json:"mythos_burrows_dug_next_COMMON"`
		KillsSiameseLynx                                    float64 `json:"kills_siamese_lynx"`
		MythosBurrowsDugCombatCOMMON                        float64 `json:"mythos_burrows_dug_combat_COMMON"`
		MythosBurrowsDugTreasureCOMMON                      float64 `json:"mythos_burrows_dug_treasure_COMMON"`
		MythosBurrowsChainsCompleteCOMMON                   float64 `json:"mythos_burrows_chains_complete_COMMON"`
		DeathsSpiritWolf                                    float64 `json:"deaths_spirit_wolf"`
		AuctionsSoldSpecial                                 float64 `json:"auctions_sold_special"`
		KillsBlueShark                                      float64 `json:"kills_blue_shark"`
		KillsNurseShark                                     float64 `json:"kills_nurse_shark"`
		KillsTigerShark                                     float64 `json:"kills_tiger_shark"`
		KillsGreatWhiteShark                                float64 `json:"kills_great_white_shark"`
		KillsTentaclees                                     float64 `json:"kills_tentaclees"`
		DeathsLividClone                                    float64 `json:"deaths_livid_clone"`
		DeathsLivid                                         float64 `json:"deaths_livid"`
		DeathsTentaclees                                    float64 `json:"deaths_tentaclees"`
		DeathsSuperArcher                                   float64 `json:"deaths_super_archer"`
		KillsMimic                                          float64 `json:"kills_mimic"`
		DeathsSadanStatue                                   float64 `json:"deaths_sadan_statue"`
		KillsSkeletorPrime                                  float64 `json:"kills_skeletor_prime"`
		DeathsSniperSkeleton                                float64 `json:"deaths_sniper_skeleton"`
		DeathsCryptWitherskeleton                           float64 `json:"deaths_crypt_witherskeleton"`
		DeathsSkeletorPrime                                 float64 `json:"deaths_skeletor_prime"`
		KillsSadanStatue                                    float64 `json:"kills_sadan_statue"`
		KillsZombieCommander                                float64 `json:"kills_zombie_commander"`
		DeathsSadanGiant                                    float64 `json:"deaths_sadan_giant"`
		DeathsSadanGolem                                    float64 `json:"deaths_sadan_golem"`
		DeathsSadan                                         float64 `json:"deaths_sadan"`
		KillsSadanGiant                                     float64 `json:"kills_sadan_giant"`
		KillsWatcherLivid                                   float64 `json:"kills_watcher_livid"`
		KillsGrinch                                         float64 `json:"kills_grinch"`
		DeathsWatcherGuardian                               float64 `json:"deaths_watcher_guardian"`
		KillsSadanGolem                                     float64 `json:"kills_sadan_golem"`
		MythosBurrowsDugNextEPIC                            float64 `json:"mythos_burrows_dug_next_EPIC"`
		MythosBurrowsDugCombatEPIC                          float64 `json:"mythos_burrows_dug_combat_EPIC"`
		KillsMinotaur                                       float64 `json:"kills_minotaur"`
		KillsMinosChampion                                  float64 `json:"kills_minos_champion"`
		MythosBurrowsDugTreasureEPIC                        float64 `json:"mythos_burrows_dug_treasure_EPIC"`
		MythosBurrowsChainsCompleteEPIC                     float64 `json:"mythos_burrows_chains_complete_EPIC"`
		KillsGaiaConstruct                                  float64 `json:"kills_gaia_construct"`
		MythosBurrowsDugNextLEGENDARY                       float64 `json:"mythos_burrows_dug_next_LEGENDARY"`
		MythosBurrowsDugCombatLEGENDARY                     float64 `json:"mythos_burrows_dug_combat_LEGENDARY"`
		DeathsMinotaur                                      float64 `json:"deaths_minotaur"`
		DeathsMinosChampion                                 float64 `json:"deaths_minos_champion"`
		MythosBurrowsDugTreasureLEGENDARY                   float64 `json:"mythos_burrows_dug_treasure_LEGENDARY"`
		MythosBurrowsChainsCompleteLEGENDARY                float64 `json:"mythos_burrows_chains_complete_LEGENDARY"`
		DeathsGaiaConstruct                                 float64 `json:"deaths_gaia_construct"`
		DeathsSiameseLynx                                   float64 `json:"deaths_siamese_lynx"`
		DeathsMinosInquisitor                               float64 `json:"deaths_minos_inquisitor"`
		KillsMinosInquisitor                                float64 `json:"kills_minos_inquisitor"`
		KillsStrongDragon                                   float64 `json:"kills_strong_dragon"`
		KillsScarecrow                                      float64 `json:"kills_scarecrow"`
		KillsWerewolf                                       float64 `json:"kills_werewolf"`
		KillsNightmare                                      float64 `json:"kills_nightmare"`
		KillsWitherGourd                                    float64 `json:"kills_wither_gourd"`
		KillsTrickOrTreater                                 float64 `json:"kills_trick_or_treater"`
		KillsWraith                                         float64 `json:"kills_wraith"`
		KillsBattyWitch                                     float64 `json:"kills_batty_witch"`
		KillsPhantomSpirit                                  float64 `json:"kills_phantom_spirit"`
		KillsScaryJerry                                     float64 `json:"kills_scary_jerry"`
		DeathsTrickOrTreater                                float64 `json:"deaths_trick_or_treater"`
		DeathsWraith                                        float64 `json:"deaths_wraith"`
		KillsPhantomFisherman                               float64 `json:"kills_phantom_fisherman"`
		KillsGrimReaper                                     float64 `json:"kills_grim_reaper"`
		KillsYeti                                           float64 `json:"kills_yeti"`
		KillsSkeletonLord                                   float64 `json:"kills_skeleton_lord"`
		KillsWitherMiner                                    float64 `json:"kills_wither_miner"`
		DeathsWitherMiner                                   float64 `json:"deaths_wither_miner"`
		KillsWitherGuard                                    float64 `json:"kills_wither_guard"`
		DeathsMaxor                                         float64 `json:"deaths_maxor"`
		KillsZombieLord                                     float64 `json:"kills_zombie_lord"`
		KillsMayorJerryBlue                                 float64 `json:"kills_mayor_jerry_blue"`
		KillsMayorJerryPurple                               float64 `json:"kills_mayor_jerry_purple"`
		KillsMayorJerryGolden                               float64 `json:"kills_mayor_jerry_golden"`
		KillsMayorJerryGreen                                float64 `json:"kills_mayor_jerry_green"`
		KillsIceWalker                                      float64 `json:"kills_ice_walker"`
		DeathsCavernsGhost                                  float64 `json:"deaths_caverns_ghost"`
		KillsGoblinWeaklingMelee                            float64 `json:"kills_goblin_weakling_melee"`
		KillsGoblinKnifeThrower                             float64 `json:"kills_goblin_knife_thrower"`
		KillsGoblinWeaklingBow                              float64 `json:"kills_goblin_weakling_bow"`
		KillsTreasureHoarder                                float64 `json:"kills_treasure_hoarder"`
		KillsGoblinBattler                                  float64 `json:"kills_goblin_battler"`
		KillsCavernsGhost                                   float64 `json:"kills_caverns_ghost"`
		KillsGoblin                                         float64 `json:"kills_goblin"`
		KillsGoblinGolem                                    float64 `json:"kills_goblin_golem"`
		KillsGoblinMurderlover                              float64 `json:"kills_goblin_murderlover"`
		KillsGoblinCreeper                                  float64 `json:"kills_goblin_creeper"`
		KillsGoblinCreepertamer                             float64 `json:"kills_goblin_creepertamer"`
		KillsPowderGhast                                    float64 `json:"kills_powder_ghast"`
		KillsCrystalSentry                                  float64 `json:"kills_crystal_sentry"`
		DeathsWatcherScarf                                  float64 `json:"deaths_watcher_scarf"`
		KillsTeamTreasuriteGrunt                            float64 `json:"kills_team_treasurite_grunt"`
		KillsTeamTreasuriteWendy                            float64 `json:"kills_team_treasurite_wendy"`
		KillsVoidlingExtremist                              float64 `json:"kills_voidling_extremist"`
		KillsVoidlingFanatic                                float64 `json:"kills_voidling_fanatic"`
		KillsCaveSpider                                     float64 `json:"kills_cave_spider"`
		DeathsGoblinMurderlover                             float64 `json:"deaths_goblin_murderlover"`
		DeathsVoidlingExtremist                             float64 `json:"deaths_voidling_extremist"`
		KillsAutomaton                                      float64 `json:"kills_automaton"`
		KillsYog                                            float64 `json:"kills_yog"`
		KillsSludge                                         float64 `json:"kills_sludge"`
		DeathsKalhuikiTribeMan                              float64 `json:"deaths_kalhuiki_tribe_man"`
		KillsTeamTreasuriteViper                            float64 `json:"kills_team_treasurite_viper"`
		KillsTeamTreasuriteSebastian                        float64 `json:"kills_team_treasurite_sebastian"`
		KillsVittomite                                      float64 `json:"kills_vittomite"`
		KillsKeyGuardian                                    float64 `json:"kills_key_guardian"`
		DeathsKalhuikiTribeWoman                            float64 `json:"deaths_kalhuiki_tribe_woman"`
		KillsButterfly                                      float64 `json:"kills_butterfly"`
		KillsFireBat                                        float64 `json:"kills_fire_bat"`
	} `json:"stats"`
	Objectives struct {
		CollectLog struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"collect_log"`
		TalkToGuide struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"talk_to_guide"`
		PublicIsland struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"public_island"`
		ExploreHub struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"explore_hub"`
		ExploreVillage struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"explore_village"`
		TalkToLibrarian struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"talk_to_librarian"`
		TalkToFarmer struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"talk_to_farmer"`
		TalkToBlacksmith struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"talk_to_blacksmith"`
		TalkToLumberjack struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"talk_to_lumberjack"`
		TalkToEventMaster struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"talk_to_event_master"`
		TalkToAuctionMaster struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"talk_to_auction_master"`
		TalkToBanker struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"talk_to_banker"`
		TalkToFairy struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"talk_to_fairy"`
		TalkToFisherman1 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"talk_to_fisherman_1"`
		MineCoal struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"mine_coal"`
		ChopTree struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"chop_tree"`
		TalkToLumberjack2 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"talk_to_lumberjack_2"`
		TalkToLazyMiner struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"talk_to_lazy_miner"`
		IncreaseMiningSkill5 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"increase_mining_skill_5"`
		WarpDeepCaverns struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"warp_deep_caverns"`
		TalkToBlacksmith2 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"talk_to_blacksmith_2"`
		TalkToLapisMiner struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"talk_to_lapis_miner"`
		TalkToLiftOperator struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"talk_to_lift_operator"`
		ReachLapisQuarry struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"reach_lapis_quarry"`
		CollectLapis struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
			INKSACK4    bool    `json:"INK_SACK:4"`
		} `json:"collect_lapis"`
		ReachPigmensDen struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"reach_pigmens_den"`
		CollectRedstone struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
			Redstone    bool    `json:"REDSTONE"`
		} `json:"collect_redstone"`
		ReachSlimehill struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"reach_slimehill"`
		CollectWheat struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"collect_wheat"`
		TalkToFarmer2 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"talk_to_farmer_2"`
		IncreaseFarmingSkill struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"increase_farming_skill"`
		WarpBarnIsland struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"warp_barn_island"`
		TalkToFarmhand1 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"talk_to_farmhand_1"`
		IncreaseFarmingSkill5 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"increase_farming_skill_5"`
		CraftWheatMinion struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"craft_wheat_minion"`
		IncreaseMiningSkill struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"increase_mining_skill"`
		ReforgeItem struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"reforge_item"`
		WarpGoldMine struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"warp_gold_mine"`
		FindPickaxe struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"find_pickaxe"`
		CollectIngots struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
			IronIngot   bool    `json:"IRON_INGOT"`
			GoldIngot   bool    `json:"GOLD_INGOT"`
		} `json:"collect_ingots"`
		TalkToFarmhand2 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"talk_to_farmhand_2"`
		CollectFarmingResources struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
			Melon       bool    `json:"MELON"`
			Pumpkin     bool    `json:"PUMPKIN"`
			PotatoItem  bool    `json:"POTATO_ITEM"`
			CarrotItem  bool    `json:"CARROT_ITEM"`
		} `json:"collect_farming_resources"`
		CollectFarmAnimalResources struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
			RawChicken  bool    `json:"RAW_CHICKEN"`
			Leather     bool    `json:"LEATHER"`
			Pork        bool    `json:"PORK"`
		} `json:"collect_farm_animal_resources"`
		CraftWorkbench struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"craft_workbench"`
		TalkToCarpenter struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"talk_to_carpenter"`
		TalkToArtist1 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"talk_to_artist_1"`
		Pafloat64Canvas struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"pafloat64_canvas"`
		CraftWoodPickaxe struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"craft_wood_pickaxe"`
		TalkToArtist2 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"talk_to_artist_2"`
		EnchantItem struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"enchant_item"`
		KillDangerMobs struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"kill_danger_mobs"`
		TalkToBartender struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"talk_to_bartender"`
		IncreaseCombatSkill struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"increase_combat_skill"`
		WarpSpidersDen struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"warp_spiders_den"`
		CollectWoolCarpenter struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"collect_wool_carpenter"`
		GiveFairySouls struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"give_fairy_souls"`
		DepositCoins struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"deposit_coins"`
		IncreaseCombatSkill5 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"increase_combat_skill_5"`
		TalkToRick struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"talk_to_rick"`
		WarpTheEnd struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"warp_the_end"`
		TalkToHaymitch struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"talk_to_haymitch"`
		CollectSpider struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
			String      bool    `json:"STRING"`
			SpiderEye   bool    `json:"SPIDER_EYE"`
		} `json:"collect_spider"`
		WarpMushroomDesert struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"warp_mushroom_desert"`
		CollectFarmingResources2 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
			SugarCane   bool    `json:"SUGAR_CANE"`
			Cactus      bool    `json:"CACTUS"`
			INKSACK3    bool    `json:"INK_SACK:3"`
		} `json:"collect_farming_resources_2"`
		CollectFarmAnimalResources2 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
			Rabbit      bool    `json:"RABBIT"`
			Mutton      bool    `json:"MUTTON"`
		} `json:"collect_farm_animal_resources_2"`
		IncreaseForagingSkill struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"increase_foraging_skill"`
		WarpForagingIslands struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"warp_foraging_islands"`
		IncreaseForagingSkill5 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"increase_foraging_skill_5"`
		TalkToGustave1 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"talk_to_gustave_1"`
		CollectBirchLogs struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"collect_birch_logs"`
		TalkToCharlie struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"talk_to_charlie"`
		CollectDarkOakLogs struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"collect_dark_oak_logs"`
		TalkToCharlie2 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"talk_to_charlie_2"`
		CollectClay struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"collect_clay"`
		TalkToFisherman2 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"talk_to_fisherman_2"`
		WarpBlazingFortress struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"warp_blazing_fortress"`
		TalkToElle struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"talk_to_elle"`
		CollectNetherResources struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
			BlazeRod    bool    `json:"BLAZE_ROD"`
			NetherStalk bool    `json:"NETHER_STALK"`
		} `json:"collect_nether_resources"`
		CollectNetherResources2 struct {
			Status        string  `json:"status"`
			Progress      float64 `json:"progress"`
			CompletedAt   float64 `json:"completed_at"`
			MagmaCream    bool    `json:"MAGMA_CREAM"`
			GlowstoneDust bool    `json:"GLOWSTONE_DUST"`
			Quartz        bool    `json:"QUARTZ"`
		} `json:"collect_nether_resources_2"`
		TalkToTelekinesisApplier struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"talk_to_telekinesis_applier"`
		CollectEmerald struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
			Emerald     bool    `json:"EMERALD"`
		} `json:"collect_emerald"`
		ReachDiamondReserve struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"reach_diamond_reserve"`
		CollectDiamond struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
			Diamond     bool    `json:"DIAMOND"`
		} `json:"collect_diamond"`
		ReachObsidianSanctuary struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"reach_obsidian_sanctuary"`
		CollectObsidian struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
			Obsidian    bool    `json:"OBSIDIAN"`
		} `json:"collect_obsidian"`
		GivePickaxeLapisMiner struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"give_pickaxe_lapis_miner"`
		GiveRickIngots struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"give_rick_ingots"`
		CompleteTheWoodsRace1 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"complete_the_woods_race_1"`
		TalkToGustave2 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"talk_to_gustave_2"`
		CompleteTheWoodsRace2 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"complete_the_woods_race_2"`
		TalkToGustave3 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"talk_to_gustave_3"`
		CompleteTheWoodsRace3 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"complete_the_woods_race_3"`
		TalkToMelody struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"talk_to_melody"`
		TalkToGuber1 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"talk_to_guber_1"`
		TalkToEndDealer struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"talk_to_end_dealer"`
		CollectEndStone struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
			EnderStone  bool    `json:"ENDER_STONE"`
		} `json:"collect_end_stone"`
		ReachDragonsNest struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"reach_dragons_nest"`
		FightDragon struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"fight_dragon"`
		CompleteTheEndRace1 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"complete_the_end_race_1"`
		TalkToGuber2 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"talk_to_guber_2"`
		CompleteTheEndRace2 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"complete_the_end_race_2"`
		TalkToGuber3 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"talk_to_guber_3"`
		CompleteTheEndRace3 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"complete_the_end_race_3"`
		TalkToFrosty struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"talk_to_frosty"`
		TalkToGulliver1 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"talk_to_gulliver_1"`
		CompleteTheChickenRace1 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"complete_the_chicken_race_1"`
		TalkToGulliver2 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"talk_to_gulliver_2"`
		CompleteTheChickenRace2 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"complete_the_chicken_race_2"`
		TalkToGulliver3 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"talk_to_gulliver_3"`
		CompleteTheChickenRace3 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"complete_the_chicken_race_3"`
		TalkToGuber4 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"talk_to_guber_4"`
		CompleteTheEndRace4 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"complete_the_end_race_4"`
		TalkToGustave4 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"talk_to_gustave_4"`
		CompleteTheWoodsRace4 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"complete_the_woods_race_4"`
		TalkToGustave5 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"talk_to_gustave_5"`
		TalkToGuber5 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"talk_to_guber_5"`
		TalkToPetCollector struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"talk_to_pet_collector"`
		TalkToPetSitter struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"talk_to_pet_sitter"`
		TalkToGulliver4 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"talk_to_gulliver_4"`
		CompleteTheChickenRace4 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"complete_the_chicken_race_4"`
		TalkToGuildford1 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"talk_to_guildford_1"`
		CompleteTheGiantMushroomAnythingWithReturnRace1 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"complete_the_giant_mushroom_anything_with_return_race_1"`
		CompleteTheGiantMushroomNoPearlsWithReturnRace1 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"complete_the_giant_mushroom_no_pearls_with_return_race_1"`
		CompleteTheGiantMushroomNoAbilitiesWithReturnRace1 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"complete_the_giant_mushroom_no_abilities_with_return_race_1"`
		CompleteTheGiantMushroomNothingWithReturnRace1 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"complete_the_giant_mushroom_nothing_with_return_race_1"`
		CompleteThePrecursorRuinsAnythingWithReturnRace1 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"complete_the_precursor_ruins_anything_with_return_race_1"`
		CompleteThePrecursorRuinsNoPearlsWithReturnRace1 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"complete_the_precursor_ruins_no_pearls_with_return_race_1"`
		CompleteThePrecursorRuinsNoAbilitiesWithReturnRace1 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"complete_the_precursor_ruins_no_abilities_with_return_race_1"`
		CompleteThePrecursorRuinsNothingWithReturnRace1 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"complete_the_precursor_ruins_nothing_with_return_race_1"`
		CompleteTheCrystalCoreAnythingWithReturnRace1 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"complete_the_crystal_core_anything_with_return_race_1"`
		CompleteTheCrystalCoreNoPearlsWithReturnRace1 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"complete_the_crystal_core_no_pearls_with_return_race_1"`
		CompleteTheCrystalCoreNoAbilitiesWithReturnRace1 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"complete_the_crystal_core_no_abilities_with_return_race_1"`
		CompleteTheCrystalCoreNothingWithReturnRace1 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"complete_the_crystal_core_nothing_with_return_race_1"`
		CompleteTheGiantMushroomAnythingNoReturnRace1 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"complete_the_giant_mushroom_anything_no_return_race_1"`
		CompleteTheGiantMushroomNoPearlsNoReturnRace1 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"complete_the_giant_mushroom_no_pearls_no_return_race_1"`
		CompleteTheGiantMushroomNoAbilitiesNoReturnRace1 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"complete_the_giant_mushroom_no_abilities_no_return_race_1"`
		CompleteTheGiantMushroomNothingNoReturnRace1 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"complete_the_giant_mushroom_nothing_no_return_race_1"`
		CompleteThePrecursorRuinsAnythingNoReturnRace1 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"complete_the_precursor_ruins_anything_no_return_race_1"`
		CompleteThePrecursorRuinsNoPearlsNoReturnRace1 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"complete_the_precursor_ruins_no_pearls_no_return_race_1"`
		CompleteThePrecursorRuinsNoAbilitiesNoReturnRace1 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"complete_the_precursor_ruins_no_abilities_no_return_race_1"`
		CompleteThePrecursorRuinsNothingNoReturnRace1 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"complete_the_precursor_ruins_nothing_no_return_race_1"`
		CompleteTheCrystalCoreAnythingNoReturnRace1 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"complete_the_crystal_core_anything_no_return_race_1"`
		CompleteTheCrystalCoreNoPearlsNoReturnRace1 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"complete_the_crystal_core_no_pearls_no_return_race_1"`
		CompleteTheCrystalCoreNoAbilitiesNoReturnRace1 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"complete_the_crystal_core_no_abilities_no_return_race_1"`
		CompleteTheCrystalCoreNothingNoReturnRace1 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"complete_the_crystal_core_nothing_no_return_race_1"`
		TalkToGuildfordCrystalCoreNothingNoReturn1 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"talk_to_guildford_crystal_core_nothing_no_return_1"`
		CompleteTheCrystalCoreNothingNoReturnRace2 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"complete_the_crystal_core_nothing_no_return_race_2"`
		TalkToGuildfordCrystalCoreNothingNoReturn2 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"talk_to_guildford_crystal_core_nothing_no_return_2"`
		CompleteTheCrystalCoreNothingNoReturnRace3 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"complete_the_crystal_core_nothing_no_return_race_3"`
		TalkToGuildfordCrystalCoreNothingNoReturn3 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"talk_to_guildford_crystal_core_nothing_no_return_3"`
		CompleteTheCrystalCoreNothingNoReturnRace4 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"complete_the_crystal_core_nothing_no_return_race_4"`
		TalkToGuildfordCrystalCoreNothingNoReturn4 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"talk_to_guildford_crystal_core_nothing_no_return_4"`
		TalkToGuildfordGiantMushroomNoAbilitiesNoReturn1 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"talk_to_guildford_giant_mushroom_no_abilities_no_return_1"`
		CompleteTheGiantMushroomNoAbilitiesNoReturnRace2 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"complete_the_giant_mushroom_no_abilities_no_return_race_2"`
		TalkToGuildfordGiantMushroomNoAbilitiesNoReturn2 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"talk_to_guildford_giant_mushroom_no_abilities_no_return_2"`
		CompleteTheGiantMushroomNoAbilitiesNoReturnRace3 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"complete_the_giant_mushroom_no_abilities_no_return_race_3"`
		TalkToGuildfordGiantMushroomNoAbilitiesNoReturn3 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"talk_to_guildford_giant_mushroom_no_abilities_no_return_3"`
		CompleteTheGiantMushroomNoAbilitiesNoReturnRace4 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"complete_the_giant_mushroom_no_abilities_no_return_race_4"`
		TalkToGuildfordGiantMushroomNoAbilitiesNoReturn4 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"talk_to_guildford_giant_mushroom_no_abilities_no_return_4"`
		TalkToGuildfordPrecursorRuinsNoAbilitiesNoReturn1 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"talk_to_guildford_precursor_ruins_no_abilities_no_return_1"`
		CompleteThePrecursorRuinsNoAbilitiesNoReturnRace2 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"complete_the_precursor_ruins_no_abilities_no_return_race_2"`
		TalkToGuildfordPrecursorRuinsNoAbilitiesNoReturn2 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"talk_to_guildford_precursor_ruins_no_abilities_no_return_2"`
		CompleteThePrecursorRuinsNoAbilitiesNoReturnRace3 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"complete_the_precursor_ruins_no_abilities_no_return_race_3"`
		TalkToGuildfordPrecursorRuinsNoAbilitiesNoReturn3 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"talk_to_guildford_precursor_ruins_no_abilities_no_return_3"`
		CompleteThePrecursorRuinsNoAbilitiesNoReturnRace4 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"complete_the_precursor_ruins_no_abilities_no_return_race_4"`
		TalkToGuildfordPrecursorRuinsNoAbilitiesNoReturn4 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"talk_to_guildford_precursor_ruins_no_abilities_no_return_4"`
		TalkToRhys struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"talk_to_rhys"`
		IncreaseMining12 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"increase_mining_12"`
		HotmGiveMaterials struct {
			Status            string  `json:"status"`
			Progress          float64 `json:"progress"`
			CompletedAt       float64 `json:"completed_at"`
			EnchantedRedstone float64 `json:"ENCHANTED_REDSTONE"`
			EnchantedIron     float64 `json:"ENCHANTED_IRON"`
			Started           bool    `json:"started"`
		} `json:"hotm_give_materials"`
		Fetchur160 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"fetchur-16-0"`
		Fetchur180 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"fetchur-18-0"`
		Fetchur190 struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"fetchur-19-0"`
		TalkToArchaeologist struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"talk_to_archaeologist"`
		TalkToShaggy struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"talk_to_shaggy"`
		TalkToGwendolyn struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"talk_to_gwendolyn"`
		TalkToBraum struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"talk_to_braum"`
		VisitGreaterMines struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"visit_greater_mines"`
		TalkToTheGoblinKing struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"talk_to_the_goblin_king"`
		KillAutomatons struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"kill_automatons"`
		EnterDivanMines struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"enter_divan_mines"`
		FindAJungleKey struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"find_a_jungle_key"`
		MineRuby struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"mine_ruby"`
		FindFourMissingPieces struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"find_four_missing_pieces"`
		CompleteTrialsOfJungleTemple struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"complete_trials_of_jungle_temple"`
		FindTheGoblinQueen struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"find_the_goblin_queen"`
		MineAmber struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"mine_amber"`
		TalkToProfessorRobot struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"talk_to_professor_robot"`
		MineSapphire struct {
			Status      string  `json:"status"`
			Progress    float64 `json:"progress"`
			CompletedAt float64 `json:"completed_at"`
		} `json:"mine_sapphire"`
	} `json:"objectives"`
	Tutorial []string `json:"tutorial"`
	Quests   struct {
		CollectLog struct {
			Status        string  `json:"status"`
			ActivatedAt   float64 `json:"activated_at"`
			ActivatedAtSb float64 `json:"activated_at_sb"`
			CompletedAt   float64 `json:"completed_at"`
			CompletedAtSb float64 `json:"completed_at_sb"`
		} `json:"collect_log"`
		ExploreHub struct {
			Status        string  `json:"status"`
			ActivatedAt   float64 `json:"activated_at"`
			ActivatedAtSb float64 `json:"activated_at_sb"`
			CompletedAt   float64 `json:"completed_at"`
			CompletedAtSb float64 `json:"completed_at_sb"`
		} `json:"explore_hub"`
		ExploreVillage struct {
			Status        string  `json:"status"`
			ActivatedAt   float64 `json:"activated_at"`
			ActivatedAtSb float64 `json:"activated_at_sb"`
			CompletedAt   float64 `json:"completed_at"`
			CompletedAtSb float64 `json:"completed_at_sb"`
		} `json:"explore_village"`
		TalkToLibrarian struct {
			Status        string  `json:"status"`
			ActivatedAt   float64 `json:"activated_at"`
			ActivatedAtSb float64 `json:"activated_at_sb"`
			CompletedAt   float64 `json:"completed_at"`
			CompletedAtSb float64 `json:"completed_at_sb"`
		} `json:"talk_to_librarian"`
		TalkToFarmer struct {
			Status        string  `json:"status"`
			ActivatedAt   float64 `json:"activated_at"`
			ActivatedAtSb float64 `json:"activated_at_sb"`
			CompletedAt   float64 `json:"completed_at"`
			CompletedAtSb float64 `json:"completed_at_sb"`
		} `json:"talk_to_farmer"`
		TalkToBlacksmith struct {
			Status        string  `json:"status"`
			ActivatedAt   float64 `json:"activated_at"`
			ActivatedAtSb float64 `json:"activated_at_sb"`
			CompletedAt   float64 `json:"completed_at"`
			CompletedAtSb float64 `json:"completed_at_sb"`
		} `json:"talk_to_blacksmith"`
		TalkToLumberjack struct {
			Status        string  `json:"status"`
			ActivatedAt   float64 `json:"activated_at"`
			ActivatedAtSb float64 `json:"activated_at_sb"`
			CompletedAt   float64 `json:"completed_at"`
			CompletedAtSb float64 `json:"completed_at_sb"`
		} `json:"talk_to_lumberjack"`
		TalkToAuctionMaster struct {
			Status        string  `json:"status"`
			ActivatedAt   float64 `json:"activated_at"`
			ActivatedAtSb float64 `json:"activated_at_sb"`
			CompletedAt   float64 `json:"completed_at"`
			CompletedAtSb float64 `json:"completed_at_sb"`
		} `json:"talk_to_auction_master"`
		TalkToBanker struct {
			Status        string  `json:"status"`
			ActivatedAt   float64 `json:"activated_at"`
			ActivatedAtSb float64 `json:"activated_at_sb"`
			CompletedAt   float64 `json:"completed_at"`
			CompletedAtSb float64 `json:"completed_at_sb"`
		} `json:"talk_to_banker"`
		TalkToLazyMiner struct {
			Status        string  `json:"status"`
			ActivatedAt   float64 `json:"activated_at"`
			ActivatedAtSb float64 `json:"activated_at_sb"`
			CompletedAt   float64 `json:"completed_at"`
			CompletedAtSb float64 `json:"completed_at_sb"`
		} `json:"talk_to_lazy_miner"`
		IncreaseMiningSkill5 struct {
			Status        string  `json:"status"`
			ActivatedAt   float64 `json:"activated_at"`
			ActivatedAtSb float64 `json:"activated_at_sb"`
			CompletedAt   float64 `json:"completed_at"`
			CompletedAtSb float64 `json:"completed_at_sb"`
		} `json:"increase_mining_skill_5"`
		TalkToLapisMiner struct {
			Status        string  `json:"status"`
			ActivatedAt   float64 `json:"activated_at"`
			ActivatedAtSb float64 `json:"activated_at_sb"`
			CompletedAt   float64 `json:"completed_at"`
			CompletedAtSb float64 `json:"completed_at_sb"`
		} `json:"talk_to_lapis_miner"`
		TalkToFarmhand1 struct {
			Status        string  `json:"status"`
			ActivatedAt   float64 `json:"activated_at"`
			ActivatedAtSb float64 `json:"activated_at_sb"`
			CompletedAt   float64 `json:"completed_at"`
			CompletedAtSb float64 `json:"completed_at_sb"`
		} `json:"talk_to_farmhand_1"`
		IncreaseFarmingSkill5 struct {
			Status        string  `json:"status"`
			ActivatedAt   float64 `json:"activated_at"`
			ActivatedAtSb float64 `json:"activated_at_sb"`
			CompletedAt   float64 `json:"completed_at"`
			CompletedAtSb float64 `json:"completed_at_sb"`
		} `json:"increase_farming_skill_5"`
		ReforgeItem struct {
			Status        string  `json:"status"`
			ActivatedAt   float64 `json:"activated_at"`
			ActivatedAtSb float64 `json:"activated_at_sb"`
			CompletedAt   float64 `json:"completed_at"`
			CompletedAtSb float64 `json:"completed_at_sb"`
		} `json:"reforge_item"`
		TalkToCarpenter struct {
			Status        string  `json:"status"`
			ActivatedAt   float64 `json:"activated_at"`
			ActivatedAtSb float64 `json:"activated_at_sb"`
			CompletedAt   float64 `json:"completed_at"`
			CompletedAtSb float64 `json:"completed_at_sb"`
		} `json:"talk_to_carpenter"`
		TalkToArtist1 struct {
			Status        string  `json:"status"`
			ActivatedAt   float64 `json:"activated_at"`
			ActivatedAtSb float64 `json:"activated_at_sb"`
			CompletedAt   float64 `json:"completed_at"`
			CompletedAtSb float64 `json:"completed_at_sb"`
		} `json:"talk_to_artist_1"`
		KillDangerMobs struct {
			Status        string  `json:"status"`
			ActivatedAt   float64 `json:"activated_at"`
			ActivatedAtSb float64 `json:"activated_at_sb"`
			CompletedAt   float64 `json:"completed_at"`
			CompletedAtSb float64 `json:"completed_at_sb"`
		} `json:"kill_danger_mobs"`
		IncreaseCombatSkill5 struct {
			Status        string  `json:"status"`
			ActivatedAt   float64 `json:"activated_at"`
			ActivatedAtSb float64 `json:"activated_at_sb"`
			CompletedAt   float64 `json:"completed_at"`
			CompletedAtSb float64 `json:"completed_at_sb"`
		} `json:"increase_combat_skill_5"`
		TalkToRick struct {
			Status        string  `json:"status"`
			ActivatedAt   float64 `json:"activated_at"`
			ActivatedAtSb float64 `json:"activated_at_sb"`
			CompletedAt   float64 `json:"completed_at"`
			CompletedAtSb float64 `json:"completed_at_sb"`
		} `json:"talk_to_rick"`
		IncreaseForagingSkill5 struct {
			Status        string  `json:"status"`
			ActivatedAt   float64 `json:"activated_at"`
			ActivatedAtSb float64 `json:"activated_at_sb"`
			CompletedAt   float64 `json:"completed_at"`
			CompletedAtSb float64 `json:"completed_at_sb"`
		} `json:"increase_foraging_skill_5"`
		TalkToGustave1 struct {
			Status        string  `json:"status"`
			ActivatedAt   float64 `json:"activated_at"`
			ActivatedAtSb float64 `json:"activated_at_sb"`
			CompletedAt   float64 `json:"completed_at"`
			CompletedAtSb float64 `json:"completed_at_sb"`
		} `json:"talk_to_gustave_1"`
		TalkToGuber1 struct {
			Status        string  `json:"status"`
			ActivatedAt   float64 `json:"activated_at"`
			ActivatedAtSb float64 `json:"activated_at_sb"`
			CompletedAt   float64 `json:"completed_at"`
			CompletedAtSb float64 `json:"completed_at_sb"`
		} `json:"talk_to_guber_1"`
		TalkToEndDealer struct {
			Status        string  `json:"status"`
			ActivatedAt   float64 `json:"activated_at"`
			ActivatedAtSb float64 `json:"activated_at_sb"`
			CompletedAt   float64 `json:"completed_at"`
			CompletedAtSb float64 `json:"completed_at_sb"`
		} `json:"talk_to_end_dealer"`
		TalkToGulliver1 struct {
			Status        string  `json:"status"`
			ActivatedAt   float64 `json:"activated_at"`
			ActivatedAtSb float64 `json:"activated_at_sb"`
			CompletedAt   float64 `json:"completed_at"`
			CompletedAtSb float64 `json:"completed_at_sb"`
		} `json:"talk_to_gulliver_1"`
		TalkToGuildford1 struct {
			Status        string  `json:"status"`
			ActivatedAt   float64 `json:"activated_at"`
			ActivatedAtSb float64 `json:"activated_at_sb"`
			CompletedAt   float64 `json:"completed_at"`
			CompletedAtSb float64 `json:"completed_at_sb"`
		} `json:"talk_to_guildford_1"`
		TalkToRhys struct {
			Status        string  `json:"status"`
			ActivatedAt   float64 `json:"activated_at"`
			ActivatedAtSb float64 `json:"activated_at_sb"`
			CompletedAt   float64 `json:"completed_at"`
			CompletedAtSb float64 `json:"completed_at_sb"`
		} `json:"talk_to_rhys"`
		TalkToArchaeologist struct {
			Status        string  `json:"status"`
			ActivatedAt   float64 `json:"activated_at"`
			ActivatedAtSb float64 `json:"activated_at_sb"`
			CompletedAt   float64 `json:"completed_at"`
			CompletedAtSb float64 `json:"completed_at_sb"`
		} `json:"talk_to_archaeologist"`
		TalkToGwendolyn struct {
			Status        string  `json:"status"`
			ActivatedAt   float64 `json:"activated_at"`
			ActivatedAtSb float64 `json:"activated_at_sb"`
			CompletedAt   float64 `json:"completed_at"`
			CompletedAtSb float64 `json:"completed_at_sb"`
		} `json:"talk_to_gwendolyn"`
		TalkToTheGoblinKing struct {
			Status        string  `json:"status"`
			ActivatedAt   float64 `json:"activated_at"`
			ActivatedAtSb float64 `json:"activated_at_sb"`
			CompletedAt   float64 `json:"completed_at"`
			CompletedAtSb float64 `json:"completed_at_sb"`
		} `json:"talk_to_the_goblin_king"`
		KillAutomatons struct {
			Status        string  `json:"status"`
			ActivatedAt   float64 `json:"activated_at"`
			ActivatedAtSb float64 `json:"activated_at_sb"`
			CompletedAt   float64 `json:"completed_at"`
			CompletedAtSb float64 `json:"completed_at_sb"`
		} `json:"kill_automatons"`
		EnterDivanMines struct {
			Status        string  `json:"status"`
			ActivatedAt   float64 `json:"activated_at"`
			ActivatedAtSb float64 `json:"activated_at_sb"`
			CompletedAt   float64 `json:"completed_at"`
			CompletedAtSb float64 `json:"completed_at_sb"`
		} `json:"enter_divan_mines"`
		FindAJungleKey struct {
			Status        string  `json:"status"`
			ActivatedAt   float64 `json:"activated_at"`
			ActivatedAtSb float64 `json:"activated_at_sb"`
			CompletedAt   float64 `json:"completed_at"`
			CompletedAtSb float64 `json:"completed_at_sb"`
		} `json:"find_a_jungle_key"`
	} `json:"quests"`
	CoinPurse                     float64  `json:"coin_purse"`
	LastDeath                     float64  `json:"last_death"`
	CraftedGenerators             []string `json:"crafted_generators"`
	VisitedZones                  []string `json:"visited_zones"`
	FairySoulsCollected           float64  `json:"fairy_souls_collected"`
	FairySouls                    float64  `json:"fairy_souls"`
	FairyExchanges                float64  `json:"fairy_exchanges"`
	FishingTreasureCaught         float64  `json:"fishing_treasure_caught"`
	DeathCount                    float64  `json:"death_count"`
	AchievementSpawnedIslandTypes []string `json:"achievement_spawned_island_types"`
	SlayerBosses                  struct {
		Zombie struct {
			ClaimedLevels struct {
				Level1        bool `json:"level_1"`
				Level2        bool `json:"level_2"`
				Level3        bool `json:"level_3"`
				Level4        bool `json:"level_4"`
				Level5        bool `json:"level_5"`
				Level6        bool `json:"level_6"`
				Level7        bool `json:"level_7"`
				Level7Special bool `json:"level_7_special"`
			} `json:"claimed_levels"`
			BossKillsTier0 float64 `json:"boss_kills_tier_0"`
			Xp             float64 `json:"xp"`
			BossKillsTier1 float64 `json:"boss_kills_tier_1"`
			BossKillsTier2 float64 `json:"boss_kills_tier_2"`
			BossKillsTier3 float64 `json:"boss_kills_tier_3"`
			BossKillsTier4 float64 `json:"boss_kills_tier_4"`
		} `json:"zombie"`
		Spider struct {
			ClaimedLevels struct {
				Level1 bool `json:"level_1"`
				Level2 bool `json:"level_2"`
				Level3 bool `json:"level_3"`
				Level4 bool `json:"level_4"`
				Level5 bool `json:"level_5"`
				Level6 bool `json:"level_6"`
				Level7 bool `json:"level_7"`
				Level8 bool `json:"level_8"`
			} `json:"claimed_levels"`
			BossKillsTier0 float64 `json:"boss_kills_tier_0"`
			Xp             float64 `json:"xp"`
			BossKillsTier1 float64 `json:"boss_kills_tier_1"`
			BossKillsTier2 float64 `json:"boss_kills_tier_2"`
			BossKillsTier3 float64 `json:"boss_kills_tier_3"`
		} `json:"spider"`
		Wolf struct {
			ClaimedLevels struct {
				Level1 bool `json:"level_1"`
				Level2 bool `json:"level_2"`
				Level3 bool `json:"level_3"`
				Level4 bool `json:"level_4"`
				Level5 bool `json:"level_5"`
				Level6 bool `json:"level_6"`
				Level7 bool `json:"level_7"`
				Level8 bool `json:"level_8"`
			} `json:"claimed_levels"`
			BossKillsTier0 float64 `json:"boss_kills_tier_0"`
			Xp             float64 `json:"xp"`
			BossKillsTier1 float64 `json:"boss_kills_tier_1"`
			BossKillsTier2 float64 `json:"boss_kills_tier_2"`
			BossKillsTier3 float64 `json:"boss_kills_tier_3"`
		} `json:"wolf"`
		Enderman struct {
			ClaimedLevels struct {
				Level1 bool `json:"level_1"`
				Level2 bool `json:"level_2"`
				Level3 bool `json:"level_3"`
				Level4 bool `json:"level_4"`
			} `json:"claimed_levels"`
			BossKillsTier0 float64 `json:"boss_kills_tier_0"`
			Xp             float64 `json:"xp"`
			BossKillsTier1 float64 `json:"boss_kills_tier_1"`
			BossKillsTier2 float64 `json:"boss_kills_tier_2"`
		} `json:"enderman"`
	} `json:"slayer_bosses"`
	Pets []struct {
		UUID      interface{} `json:"uuid"`
		Type      string      `json:"type"`
		Exp       float64     `json:"exp"`
		Active    bool        `json:"active"`
		Tier      string      `json:"tier"`
		HeldItem  interface{} `json:"heldItem"`
		CandyUsed float64     `json:"candyUsed"`
		Skin      interface{} `json:"skin"`
	} `json:"pets"`
	Griffin struct {
		Burrows []struct {
			Ts    float64 `json:"ts"`
			X     float64 `json:"x"`
			Y     float64 `json:"y"`
			Z     float64 `json:"z"`
			Type  float64 `json:"type"`
			Tier  float64 `json:"tier"`
			Chain float64 `json:"chain"`
		} `json:"burrows"`
	} `json:"griffin"`
	Experimentation struct {
		Simon struct {
			LastAttempt float64 `json:"last_attempt"`
			Attempts2   float64 `json:"attempts_2"`
			BonusClicks float64 `json:"bonus_clicks"`
			LastClaimed float64 `json:"last_claimed"`
			Claims2     float64 `json:"claims_2"`
			BestScore2  float64 `json:"best_score_2"`
			Attempts3   float64 `json:"attempts_3"`
			Claims3     float64 `json:"claims_3"`
			BestScore3  float64 `json:"best_score_3"`
			Attempts5   float64 `json:"attempts_5"`
			Claims5     float64 `json:"claims_5"`
			BestScore5  float64 `json:"best_score_5"`
		} `json:"simon"`
		Pairings struct {
			LastClaimed float64 `json:"last_claimed"`
			Claims3     float64 `json:"claims_3"`
			BestScore3  float64 `json:"best_score_3"`
			LastAttempt float64 `json:"last_attempt"`
			Claims4     float64 `json:"claims_4"`
			BestScore4  float64 `json:"best_score_4"`
			Claims5     float64 `json:"claims_5"`
			BestScore5  float64 `json:"best_score_5"`
		} `json:"pairings"`
		Numbers struct {
			LastAttempt float64 `json:"last_attempt"`
			Attempts2   float64 `json:"attempts_2"`
			BonusClicks float64 `json:"bonus_clicks"`
			LastClaimed float64 `json:"last_claimed"`
			Claims2     float64 `json:"claims_2"`
			BestScore2  float64 `json:"best_score_2"`
			Attempts1   float64 `json:"attempts_1"`
			Claims1     float64 `json:"claims_1"`
			BestScore1  float64 `json:"best_score_1"`
			Attempts3   float64 `json:"attempts_3"`
			Claims3     float64 `json:"claims_3"`
			BestScore3  float64 `json:"best_score_3"`
		} `json:"numbers"`
		ClaimsResets          float64 `json:"claims_resets"`
		ClaimsResetsTimestamp float64 `json:"claims_resets_timestamp"`
	} `json:"experimentation"`
	Perks struct {
		CatacombsBossLuck   float64 `json:"catacombs_boss_luck"`
		CatacombsHealth     float64 `json:"catacombs_health"`
		CatacombsStrength   float64 `json:"catacombs_strength"`
		CatacombsCritDamage float64 `json:"catacombs_crit_damage"`
		PermanentDefense    float64 `json:"permanent_defense"`
		PermanentStrength   float64 `json:"permanent_strength"`
		CatacombsLooting    float64 `json:"catacombs_looting"`
		CatacombsDefense    float64 `json:"catacombs_defense"`
		ForbiddenBlessing   float64 `json:"forbidden_blessing"`
		PermanentHealth     float64 `json:"permanent_health"`
		PermanentSpeed      float64 `json:"permanent_speed"`
	} `json:"perks"`
	HarpQuest struct {
		SelectedSong                          string  `json:"selected_song"`
		SelectedSongEpoch                     float64 `json:"selected_song_epoch"`
		SongHymnJoyCompletions                float64 `json:"song_hymn_joy_completions"`
		SongHymnJoyBestCompletion             float64 `json:"song_hymn_joy_best_completion"`
		SongHymnJoyPerfectCompletions         float64 `json:"song_hymn_joy_perfect_completions"`
		SongFrereJacquesBestCompletion        float64 `json:"song_frere_jacques_best_completion"`
		SongFrereJacquesCompletions           float64 `json:"song_frere_jacques_completions"`
		SongAmazingGraceBestCompletion        float64 `json:"song_amazing_grace_best_completion"`
		SongAmazingGraceCompletions           float64 `json:"song_amazing_grace_completions"`
		SongFrereJacquesPerfectCompletions    float64 `json:"song_frere_jacques_perfect_completions"`
		SongAmazingGracePerfectCompletions    float64 `json:"song_amazing_grace_perfect_completions"`
		SongBrahmsBestCompletion              float64 `json:"song_brahms_best_completion"`
		SongBrahmsCompletions                 float64 `json:"song_brahms_completions"`
		SongBrahmsPerfectCompletions          float64 `json:"song_brahms_perfect_completions"`
		SongHappyBirthdayBestCompletion       float64 `json:"song_happy_birthday_best_completion"`
		SongHappyBirthdayCompletions          float64 `json:"song_happy_birthday_completions"`
		SongHappyBirthdayPerfectCompletions   float64 `json:"song_happy_birthday_perfect_completions"`
		SongGreensleevesBestCompletion        float64 `json:"song_greensleeves_best_completion"`
		SongGreensleevesCompletions           float64 `json:"song_greensleeves_completions"`
		SongGreensleevesPerfectCompletions    float64 `json:"song_greensleeves_perfect_completions"`
		SongJeopardyBestCompletion            float64 `json:"song_jeopardy_best_completion"`
		SongJeopardyCompletions               float64 `json:"song_jeopardy_completions"`
		SongJeopardyPerfectCompletions        float64 `json:"song_jeopardy_perfect_completions"`
		SongMinuetBestCompletion              float64 `json:"song_minuet_best_completion"`
		SongMinuetCompletions                 float64 `json:"song_minuet_completions"`
		SongMinuetPerfectCompletions          float64 `json:"song_minuet_perfect_completions"`
		SongJoyWorldBestCompletion            float64 `json:"song_joy_world_best_completion"`
		SongJoyWorldCompletions               float64 `json:"song_joy_world_completions"`
		SongJoyWorldPerfectCompletions        float64 `json:"song_joy_world_perfect_completions"`
		SongPureImaginationBestCompletion     float64 `json:"song_pure_imagination_best_completion"`
		SongPureImaginationCompletions        float64 `json:"song_pure_imagination_completions"`
		SongPureImaginationPerfectCompletions float64 `json:"song_pure_imagination_perfect_completions"`
		SongVieEnRoseBestCompletion           float64 `json:"song_vie_en_rose_best_completion"`
		SongVieEnRoseCompletions              float64 `json:"song_vie_en_rose_completions"`
		SongVieEnRosePerfectCompletions       float64 `json:"song_vie_en_rose_perfect_completions"`
		ClaimedTalisman                       bool    `json:"claimed_talisman"`
		SongFireAndFlamesCompletions          float64 `json:"song_fire_and_flames_completions"`
	} `json:"harp_quest"`
	ActiveEffects []struct {
		Effect         string        `json:"effect"`
		Level          float64       `json:"level"`
		Modifiers      []interface{} `json:"modifiers"`
		TicksRemaining float64       `json:"ticks_remaining"`
		Infinite       bool          `json:"infinite"`
	} `json:"active_effects"`
	PausedEffects         []interface{} `json:"paused_effects"`
	DisabledPotionEffects []string      `json:"disabled_potion_effects"`
	VisitedModes          []string      `json:"visited_modes"`
	TempStatBuffs         []struct {
		Stat     float64 `json:"stat"`
		Key      string  `json:"key"`
		Amount   float64 `json:"amount"`
		ExpireAt float64 `json:"expire_at"`
	} `json:"temp_stat_buffs"`
	MiningCore struct {
		Nodes struct {
			MiningSpeed      float64 `json:"mining_speed"`
			MiningFortune    float64 `json:"mining_fortune"`
			TitaniumInsanium float64 `json:"titanium_insanium"`
			MiningSpeedBoost float64 `json:"mining_speed_boost"`
			RandomEvent      float64 `json:"random_event"`
			MiningMadness    float64 `json:"mining_madness"`
			MiningExperience float64 `json:"mining_experience"`
		} `json:"nodes"`
		ReceivedFreeTier       bool    `json:"received_free_tier"`
		Tokens                 float64 `json:"tokens"`
		TokensSpent            float64 `json:"tokens_spent"`
		PowderMithril          float64 `json:"powder_mithril"`
		PowderMithrilTotal     float64 `json:"powder_mithril_total"`
		PowderSpentMithril     float64 `json:"powder_spent_mithril"`
		Experience             float64 `json:"experience"`
		SelectedPickaxeAbility string  `json:"selected_pickaxe_ability"`
		RetroactiveTier2Token  bool    `json:"retroactive_tier2_token"`
		Crystals               struct {
			JadeCrystal struct {
			} `json:"jade_crystal"`
			AmberCrystal struct {
				State       string  `json:"state"`
				TotalPlaced float64 `json:"total_placed"`
			} `json:"amber_crystal"`
			AmethystCrystal struct {
			} `json:"amethyst_crystal"`
			SapphireCrystal struct {
				State       string  `json:"state"`
				TotalPlaced float64 `json:"total_placed"`
			} `json:"sapphire_crystal"`
			TopazCrystal struct {
				State       string  `json:"state"`
				TotalPlaced float64 `json:"total_placed"`
			} `json:"topaz_crystal"`
			JasperCrystal struct {
			} `json:"jasper_crystal"`
			RubyCrystal struct {
			} `json:"ruby_crystal"`
		} `json:"crystals"`
		GreaterMinesLastAccess float64 `json:"greater_mines_last_access"`
		Biomes                 struct {
			Precursor struct {
			} `json:"precursor"`
			Dwarven struct {
				StatuesPlaced []interface{} `json:"statues_placed"`
			} `json:"dwarven"`
			Goblin struct {
				KingQuestActive     bool    `json:"king_quest_active"`
				KingQuestsCompleted float64 `json:"king_quests_completed"`
			} `json:"goblin"`
		} `json:"biomes"`
		PowderGemstone      float64 `json:"powder_gemstone"`
		PowderGemstoneTotal float64 `json:"powder_gemstone_total"`
	} `json:"mining_core"`
	Forge struct {
		ForgeProcesses struct {
			Forge1 struct {
			} `json:"forge_1"`
		} `json:"forge_processes"`
	} `json:"forge"`
	ExperienceSkillRunecrafting float64  `json:"experience_skill_runecrafting"`
	ExperienceSkillMining       float64  `json:"experience_skill_mining"`
	UnlockedCollTiers           []string `json:"unlocked_coll_tiers"`
	ExperienceSkillAlchemy      float64  `json:"experience_skill_alchemy"`
	BackpackContents            map[string]struct {
		Type float64 `json:"type"`
		Data string  `json:"data"`
	} `json:"backpack_contents"`
	Quiver struct {
		Type float64 `json:"type"`
		Data string  `json:"data"`
	} `json:"quiver"`
	ExperienceSkillSocial float64 `json:"experience_skill_social"`
	ExperienceSkillTaming float64 `json:"experience_skill_taming"`
	SacksCounts           struct {
		TarantulaWeb       float64 `json:"TARANTULA_WEB"`
		RevenantFlesh      float64 `json:"REVENANT_FLESH"`
		WolfTooth          float64 `json:"WOLF_TOOTH"`
		RawFish            float64 `json:"RAW_FISH"`
		WaterLily          float64 `json:"WATER_LILY"`
		RAWFISH1           float64 `json:"RAW_FISH:1"`
		InkSack            float64 `json:"INK_SACK"`
		ClayBall           float64 `json:"CLAY_BALL"`
		Sponge             float64 `json:"SPONGE"`
		RAWFISH2           float64 `json:"RAW_FISH:2"`
		PrismarineCrystals float64 `json:"PRISMARINE_CRYSTALS"`
		PrismarineShard    float64 `json:"PRISMARINE_SHARD"`
		RAWFISH3           float64 `json:"RAW_FISH:3"`
		RottenFlesh        float64 `json:"ROTTEN_FLESH"`
		Bone               float64 `json:"BONE"`
		MagmaCream         float64 `json:"MAGMA_CREAM"`
		SpiderEye          float64 `json:"SPIDER_EYE"`
		String             float64 `json:"STRING"`
		SlimeBall          float64 `json:"SLIME_BALL"`
		EnderPearl         float64 `json:"ENDER_PEARL"`
		BlazeRod           float64 `json:"BLAZE_ROD"`
		Sulphur            float64 `json:"SULPHUR"`
		DungeonTrap        float64 `json:"DUNGEON_TRAP"`
		DungeonDecoy       float64 `json:"DUNGEON_DECOY"`
		InflatableJerry    float64 `json:"INFLATABLE_JERRY"`
		SpiritLeap         float64 `json:"SPIRIT_LEAP"`
		IronIngot          float64 `json:"IRON_INGOT"`
		Coal               float64 `json:"COAL"`
		GoldIngot          float64 `json:"GOLD_INGOT"`
		Cobblestone        float64 `json:"COBBLESTONE"`
		INKSACK4           float64 `json:"INK_SACK:4"`
		Redstone           float64 `json:"REDSTONE"`
		Ice                float64 `json:"ICE"`
		Diamond            float64 `json:"DIAMOND"`
		Obsidian           float64 `json:"OBSIDIAN"`
		EnderStone         float64 `json:"ENDER_STONE"`
		GhastTear          float64 `json:"GHAST_TEAR"`
		Sand               float64 `json:"SAND"`
		Emerald            float64 `json:"EMERALD"`
		MithrilOre         float64 `json:"MITHRIL_ORE"`
		TitaniumOre        float64 `json:"TITANIUM_ORE"`
		Starfall           float64 `json:"STARFALL"`
		Treasurite         float64 `json:"TREASURITE"`
		Quartz             float64 `json:"QUARTZ"`
		HardStone          float64 `json:"HARD_STONE"`
		GreenCandy         float64 `json:"GREEN_CANDY"`
		PurpleCandy        float64 `json:"PURPLE_CANDY"`
		NetherStalk        float64 `json:"NETHER_STALK"`
		PotatoItem         float64 `json:"POTATO_ITEM"`
		Gravel             float64 `json:"GRAVEL"`
		CarrotItem         float64 `json:"CARROT_ITEM"`
		Netherrack         float64 `json:"NETHERRACK"`
		WhiteGift          float64 `json:"WHITE_GIFT"`
		NullSphere         float64 `json:"NULL_SPHERE"`
		RuneGem1           float64 `json:"RUNE_GEM_1"`
		RuneClouds1        float64 `json:"RUNE_CLOUDS_1"`
		Seeds              float64 `json:"SEEDS"`
	} `json:"sacks_counts"`
	EssenceUndead float64 `json:"essence_undead"`
	TalismanBag   struct {
		Type float64 `json:"type"`
		Data string  `json:"data"`
	} `json:"talisman_bag"`
	BackpackIcons struct {
		Num0 struct {
			Type float64 `json:"type"`
			Data string  `json:"data"`
		} `json:"0"`
		Num1 struct {
			Type float64 `json:"type"`
			Data string  `json:"data"`
		} `json:"1"`
	} `json:"backpack_icons"`
	ExperienceSkillCombat float64 `json:"experience_skill_combat"`
	EssenceDiamond        float64 `json:"essence_diamond"`
	FishingBag            struct {
		Type float64 `json:"type"`
		Data string  `json:"data"`
	} `json:"fishing_bag"`
	ExperienceSkillFarming float64 `json:"experience_skill_farming"`
	WardrobeEquippedSlot   float64 `json:"wardrobe_equipped_slot"`
	Collection             struct {
		Seeds              float64 `json:"SEEDS"`
		Log                float64 `json:"LOG"`
		Cobblestone        float64 `json:"COBBLESTONE"`
		Coal               float64 `json:"COAL"`
		IronIngot          float64 `json:"IRON_INGOT"`
		SlimeBall          float64 `json:"SLIME_BALL"`
		Sulphur            float64 `json:"SULPHUR"`
		INKSACK4           float64 `json:"INK_SACK:4"`
		RottenFlesh        float64 `json:"ROTTEN_FLESH"`
		Redstone           float64 `json:"REDSTONE"`
		Wheat              float64 `json:"WHEAT"`
		Pork               float64 `json:"PORK"`
		RawChicken         float64 `json:"RAW_CHICKEN"`
		Leather            float64 `json:"LEATHER"`
		GoldIngot          float64 `json:"GOLD_INGOT"`
		Melon              float64 `json:"MELON"`
		Pumpkin            float64 `json:"PUMPKIN"`
		PotatoItem         float64 `json:"POTATO_ITEM"`
		CarrotItem         float64 `json:"CARROT_ITEM"`
		Bone               float64 `json:"BONE"`
		String             float64 `json:"STRING"`
		Mutton             float64 `json:"MUTTON"`
		Rabbit             float64 `json:"RABBIT"`
		SugarCane          float64 `json:"SUGAR_CANE"`
		RawFish            float64 `json:"RAW_FISH"`
		Cactus             float64 `json:"CACTUS"`
		MushroomCollection float64 `json:"MUSHROOM_COLLECTION"`
		INKSACK3           float64 `json:"INK_SACK:3"`
		EnderPearl         float64 `json:"ENDER_PEARL"`
		SpiderEye          float64 `json:"SPIDER_EYE"`
		LOG2               float64 `json:"LOG:2"`
		LOG1               float64 `json:"LOG:1"`
		LOG3               float64 `json:"LOG:3"`
		Log2               float64 `json:"LOG_2"`
		LOG21              float64 `json:"LOG_2:1"`
		ClayBall           float64 `json:"CLAY_BALL"`
		RAWFISH1           float64 `json:"RAW_FISH:1"`
		WaterLily          float64 `json:"WATER_LILY"`
		InkSack            float64 `json:"INK_SACK"`
		RAWFISH3           float64 `json:"RAW_FISH:3"`
		RAWFISH2           float64 `json:"RAW_FISH:2"`
		PrismarineShard    float64 `json:"PRISMARINE_SHARD"`
		MagmaCream         float64 `json:"MAGMA_CREAM"`
		NetherStalk        float64 `json:"NETHER_STALK"`
		BlazeRod           float64 `json:"BLAZE_ROD"`
		GlowstoneDust      float64 `json:"GLOWSTONE_DUST"`
		Quartz             float64 `json:"QUARTZ"`
		Diamond            float64 `json:"DIAMOND"`
		Emerald            float64 `json:"EMERALD"`
		Obsidian           float64 `json:"OBSIDIAN"`
		Gravel             float64 `json:"GRAVEL"`
		Feather            float64 `json:"FEATHER"`
		Netherrack         float64 `json:"NETHERRACK"`
		PrismarineCrystals float64 `json:"PRISMARINE_CRYSTALS"`
		GhastTear          float64 `json:"GHAST_TEAR"`
		EnderStone         float64 `json:"ENDER_STONE"`
		Sponge             float64 `json:"SPONGE"`
		Ice                float64 `json:"ICE"`
		Sand               float64 `json:"SAND"`
		EnchantedDiamond   float64 `json:"ENCHANTED_DIAMOND"`
		EnchantedGold      float64 `json:"ENCHANTED_GOLD"`
		MithrilOre         float64 `json:"MITHRIL_ORE"`
		REDROSE7           float64 `json:"RED_ROSE:7"`
		Wool               float64 `json:"WOOL"`
		HardStone          float64 `json:"HARD_STONE"`
		GemstoneCollection float64 `json:"GEMSTONE_COLLECTION"`
	} `json:"collection"`
	EssenceDragon      float64 `json:"essence_dragon"`
	EssenceGold        float64 `json:"essence_gold"`
	EnderChestContents struct {
		Type float64 `json:"type"`
		Data string  `json:"data"`
	} `json:"ender_chest_contents"`
	WardrobeContents struct {
		Type float64 `json:"type"`
		Data string  `json:"data"`
	} `json:"wardrobe_contents"`
	PotionBag struct {
		Type float64 `json:"type"`
		Data string  `json:"data"`
	} `json:"potion_bag"`
	ExperienceSkillEnchanting float64 `json:"experience_skill_enchanting"`
	PersonalVaultContents     struct {
		Type float64 `json:"type"`
		Data string  `json:"data"`
	} `json:"personal_vault_contents"`
	ExperienceSkillFishing float64 `json:"experience_skill_fishing"`
	InvContents            struct {
		Type float64 `json:"type"`
		Data string  `json:"data"`
	} `json:"inv_contents"`
	EssenceIce              float64 `json:"essence_ice"`
	EssenceWither           float64 `json:"essence_wither"`
	EssenceSpider           float64 `json:"essence_spider"`
	ExperienceSkillForaging float64 `json:"experience_skill_foraging"`
	CandyInventoryContents  struct {
		Type float64 `json:"type"`
		Data string  `json:"data"`
	} `json:"candy_inventory_contents"`
	ExperienceSkillCarpentry float64 `json:"experience_skill_carpentry"`
}
