package util

type NetworthReturn struct {
	Data   Data `json:"data"`
	Status int  `json:"status"`
}
type Storage struct {
	TopItems []TopItems `json:"top_items"`
	Total    int        `json:"total"`
}
type Inventory struct {
	TopItems []TopItems `json:"top_items"`
	Total    int        `json:"total"`
}
type Armor struct {
	TopItems []TopItems `json:"top_items"`
	Total    int        `json:"total"`
}
type WardrobeInventory struct {
	TopItems []TopItems `json:"top_items"`
	Total    int        `json:"total"`
}
type Pets struct {
	TopItems []TopItems `json:"top_items"`
	Total    int        `json:"total"`
}
type TopItems struct {
	ID     string `json:"id"`
	Name   string `json:"name"`
	Price  int    `json:"price"`
	Recomb bool   `json:"recomb"`
	Count  int    `json:"count"`
}
type Talismans struct {
	TopItems []TopItems `json:"top_items"`
	Total    int        `json:"total"`
}
type Categories struct {
	Storage           Storage           `json:"storage"`
	Inventory         Inventory         `json:"inventory"`
	Armor             Armor             `json:"armor"`
	WardrobeInventory WardrobeInventory `json:"wardrobe_inventory"`
	Pets              Pets              `json:"pets"`
	Talismans         Talismans         `json:"talismans"`
}
type Data struct {
	Categories Categories `json:"categories"`
	Bank       float64    `json:"bank"`
	Purse      float64    `json:"purse"`
	Sacks      float64    `json:"sacks"`
	Networth   float64    `json:"networth"`
}
