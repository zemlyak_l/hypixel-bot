package util

import (
	"fmt"
	"time"
)

// huge fucking player struct

type PlayerResponse struct {
	Player  Player `json:"player"`
	Success bool   `json:"success"`
}

type Player struct {
	UUID           string   `json:"uuid"`
	Displayname    string   `json:"displayname"`
	Playername     string   `json:"playername"`
	FriendRequests []string `json:"friendRequests"`
	KnownAliases   []string `json:"knownAliases"`
	Stats          *struct {
		UUID    string   `json:"uuid"`
		SkyWars *SkyWars `json:"SkyWars"`
		BedWars *BedWars `json:"BedWars"`
	}
	Achievements struct {
		Bedwars_Level int `json:"bedwars_level"`
	}
	Karma      int     `json:"karma"`
	LastLogin  int64   `json:"lastLogin"`
	FirstLogin int64   `json:"firstLogin"`
	NetworkExp float32 `json:"networkExp"`
}

type BedWars struct {
	Wins         float32 `json:"wins_bedwars"`
	Games        int     `json:"games_played_bedwars"`
	Losses       float32 `json:"losses_bedwars"`
	Final_Kills  float32 `json:"final_kills_bedwars"`
	Final_Deaths float32 `json:"final_deaths_bedwars"`
	Beds_Lost    float32 `json:"beds_lost_bedwars"`
	Beds_Broken  float32 `json:"beds_broken_bedwars"`
	Winstreak    int     `json:"winstreak"`
}

func (bw *BedWars) WinLoss() string {
	return fmt.Sprintf("%.2f", bw.Wins/bw.Losses)
}

func (bw *BedWars) FinalKD() string {
	return fmt.Sprintf("%.2f", bw.Final_Kills/bw.Final_Deaths)
}

func (bw *BedWars) BedsBL() string {
	return fmt.Sprintf("%.2f", bw.Beds_Broken/bw.Beds_Lost)
}

type swlvl string
type SkyWars struct {
	GamesPlayedSkywars int     `json:"games_played_skywars"`
	Kills              float32 `json:"kills"`
	Level              swlvl   `json:"levelFormatted"`
	Deaths             float32 `json:"deaths"`
	BlocksPlaced       int     `json:"blocks_placed"`
	Coins              int     `json:"coins"`
	Losses             float32 `json:"losses"`
	MostKillsGame      int     `json:"most_kills_game"`
	Souls              int     `json:"souls"`
	BlocksBroken       int     `json:"blocks_broken"`
	Assists            int     `json:"assists"`
	Games              int     `json:"games"`
	ArrowsHit          int     `json:"arrows_hit"`
	FastestWin         int     `json:"fastest_win"`
	Wins               float32 `json:"wins"`
	Winstreak          int     `json:"winstreak"`
}

func (sw *SkyWars) WinLoss() string {
	return fmt.Sprintf("%.2f", sw.Wins/sw.Losses)
}

func (sw *SkyWars) KillDeath() string {
	return fmt.Sprintf("%.2f", sw.Kills/sw.Deaths)
}

func (lvl swlvl) String() string {
	if len(lvl) != 0 {
		return string(lvl)[3:]
	}

	return ""
}

// mojang api response

type Mojang struct {
	Name string `json:"name"`
	Id   string `json:"id"`
}

type Name struct {
	Name string `json:"name"`
}

// status endpoint

type Status struct {
	Success bool   `json:"success"`
	UUID    string `json:"uuid"`

	Session struct {
		Online   bool   `json:"online"`
		GameType string `json:"gameType"`
		Mode     string `json:"mode"`
	}
}

type Status2 struct {
	Success bool `json:"success"`

	Player struct {
		LastLogin  int64 `json:"lastLogin"`
		LastLogout int64 `json:"lastLogout"`
	}
}

// skyblock shit god bless

type Profile struct {
	Id       string `json:"id"`
	Gamemode string `json:"game_mode"`
	CuteName string `json:"cute_name"`
	Banking  struct {
		Balance float32 `json:"balance"`
	} `json:"banking"`

	Members map[string]Member `json:"members"`
}

type ProfileReturn struct {
	Success  bool      `json:"success"`
	Profiles []Profile `json:"profiles"`
}

type Skill struct {
	Xp         float32 `json:"xp"`
	Level      int     `json:"level"`
	FloatLevel float32 `json:"floatLevel"`
	MaxLevel   int     `json:"maxLevel"`
	XpCurrent  int     `json:"xpCurrent"`
	XpForNext  int     `json:"xpForNext"`
	Progress   float32 `json:"progress"`
}

func (s Skill) String() string {
	return fmt.Sprintf("%d/50 (%.0f%%)", s.Level, s.Progress)
}

type Slayer struct {
	Claimed   int `json:"claimed_levels"`
	Xp        int `json:"xp"`
	XpForNext int `json:"xp_for_next"`
}

func (s Slayer) Level() (level int) {
	for i, x := range Slayer_xp {
		if x > s.Xp {
			break
		} else {
			level = i
		}
	}

	return level
}

func (s Slayer) String() string {
	return fmt.Sprintf("")
}

type Attribute float32

type Member struct {
	Purse  float32 `json:"coin_purse"`
	Skills struct {
		Mining       Skill `json:"mining"`
		Runecrafting Skill `json:"runecrafting"`
		Alchemy      Skill `json:"alchemy"`
		Taming       Skill `json:"taming"`
		Combat       Skill `json:"combat"`
		Farming      Skill `json:"farming"`
		Enchanting   Skill `json:"enchanting"`
		Fishing      Skill `json:"fishing"`
		Foraging     Skill `json:"foraging"`
		Carpentry    Skill `json:"carpentry"`
	} `json:"skills"`
	Average float32 `json:"average_skill_level"`

	Slayers Slayers `json:"slayer"`

	Attributes struct {
		Damage            Attribute `json:"damage"`
		Health            Attribute `json:"health"`
		Defense           Attribute `json:"defense"`
		EffectiveHealth   Attribute `json:"effective_health"`
		Strength          Attribute `json:"strength"`
		DamageIncrease    Attribute `json:"damage_increase"`
		Speed             Attribute `json:"speed"`
		CritChance        Attribute `json:"crit_chance"`
		CritDamage        Attribute `json:"crit_damage"`
		BonusAttackSpeed  Attribute `json:"bonus_attack_speed"`
		Intelligence      Attribute `json:"intelligence"`
		SeaCreatureChance Attribute `json:"sea_creature_chance"`
		MagicFind         Attribute `json:"magic_find"`
		PetLuck           Attribute `json:"pet_luck"`
		TrueDefense       Attribute `json:"true_defense"`
		Ferocity          Attribute `json:"ferocity"`
		AbilityDamage     Attribute `json:"ability_damage"`
		MiningSpeed       Attribute `json:"mining_speed"`
		MiningFortune     Attribute `json:"mining_fortune"`
		FarmingFortune    Attribute `json:"farming_fortune"`
		ForagingFortune   Attribute `json:"foraging_fortune"`
	} `json:"attributes"`
}

type Slayers struct {
	Zombie   Slayer `json:"zombie"`
	Wolf     Slayer `json:"wolf"`
	Spider   Slayer `json:"spider"`
	Enderman Slayer `json:"enderman"`
}

func (s *Slayers) String() string {
	return fmt.Sprintf("Уровень: %d | %d | %d | %d\nXP: %d | %d | %d | %d",
		s.Zombie.Level(), s.Spider.Level(), s.Wolf.Level(), s.Enderman.Level(),
		s.Zombie.Xp, s.Spider.Xp, s.Wolf.Xp, s.Enderman.Xp,
	)
}

func (s *Slayers) TotalXP() int {
	return s.Zombie.Xp + s.Spider.Xp + s.Wolf.Xp + s.Enderman.Xp
}

// Bazaar structs

type BazaarData struct {
	Success     bool
	LastUpdated int
	Products    map[string]Product
}

type Product struct {
	ID          string        `json:"product_id"`
	SellSummary []SummaryInfo `json:"sell_summary"`
	BuySummary  []SummaryInfo `json:"buy_summary"`
	QuickStatus QuickStatus   `json:"quick_status"`
}

type SummaryInfo struct {
	Amount       int
	PricePerUnit float64
	Orders       int
}

type QuickStatus struct {
	SellPrice      float64
	SellVolume     int
	SellMovingWeek int
	SellOrders     int
	BuyPrice       float64
	BuyVolume      int
	BuyMovingWeek  int
	BuyOrders      int
}

// auction data

type AuctionReturn struct {
	Success       bool
	Page          int
	TotalPages    int
	TotalAuctions int
	LastUpdated   int
	Auctions      []AuctionData
}

type AuctionCache struct {
	LastUpdated time.Time
	Auctions    []AuctionData
}

type AuctionData struct {
	ID          string    `json:"uuid"`
	Auctioneer  string    `json:"auctioneer"`
	ProfileID   string    `json:"profile_id"`
	Start       int       `json:"start"`
	End         int       `json:"end"`
	Name        string    `json:"item_name"`
	Lore        string    `json:"item_lore"`
	Tier        string    `json:"tier"`
	StartingBid int       `json:"starting_bid"`
	Claimed     bool      `json:"claimed"`
	HighestBid  int       `json:"highest_bid_amount"`
	BIN         bool      `json:"bin"`
	Bids        []BidData `json:"bids"`
}

type BidData struct {
	AuctionID string `json:"auction_id" gorm:"primary_key;autoIncrement:false"`
	Bidder    string
	ProfileID string `json:"profile_id"`
	Amount    int
	Timestamp int `gorm:"primary_key;autoIncrement:false"`
}

type EndedAuctionReturn struct {
	Success     bool
	LastUpdated int
	Auctions    []EndedAuction
}

type EndedAuction struct {
	AuctionID     string `json:"auction_id"`
	Seller        string
	SellerProfile string
	Buyer         string
	Timestamp     int
	Price         int
	BIN           bool `json:"bin"`
}

func (auction *AuctionData) GetHighestBid() BidData {
	highest := BidData{Amount: 0}
	for _, bid := range auction.Bids {
		if bid.Amount > highest.Amount {
			highest = bid
		}
	}
	return highest
}

type RankedResponse struct {
	Success bool `json:"success"`
	Ranked  struct {
		Key      string `json:"key"`
		Position int    `json:"position"`
		Score    int    `json:"score"`
	} `json:"result"`
}
