package commands

import (
	"bytes"
	"encoding/json"
	"fmt"
	"image"
	"image/color"
	"image/draw"
	"image/png"
	"regexp"
	"strings"
	"sync"
	"text/template"

	"hypixel-bot/pkg/bot"
	"hypixel-bot/pkg/util"

	"github.com/SevereCloud/vksdk/v2/api"
	"github.com/golang/freetype"
	"github.com/golang/freetype/truetype"
)

var bg, fg = color.RGBA{46, 52, 64, 255}, color.RGBA{129, 161, 193, 255}

var Encoder = png.Encoder{CompressionLevel: -3}
var f, _ = truetype.Parse(util.Font)
var opts = truetype.Options{Size: 12}

var sbtmpl = template.Must(template.ParseFiles("tmpl/sb.gohtml"))

var Skyblock = &Command{
	Name:      regexp.MustCompile("^(skyblock|сб|скайблок|sb)$"),
	Args:      1,
	ForAdmins: false,
	UseDBName: true,
	Trigger: func(args []string, peer_id int, from_id int) (err error) {
		mojang, err := util.GetUUID(args[0])
		if err != nil {
			return
		}
		profile := &util.Profile{}

		var wg sync.WaitGroup
		wg.Add(2)

		go func() {
			_, res, err := util.Client.Get(nil, "https://api.slothpixel.me/api/skyblock/profile/"+mojang.Id+"?key="+bot.HypixelKey)
			if err != nil {
				return
			}
			err = json.Unmarshal([]byte(res), &profile)
			if err != nil {
				return
			}
			wg.Done()
		}()

		var skin image.Image
		go func() {
			_, res, err := util.Client.Get(nil, "https://visage.surgeplay.com/full/448/"+mojang.Id)
			if err != nil {
				return
			}
			skin, _, err = image.Decode(bytes.NewReader(res))
			if err != nil {
				return
			}
			wg.Done()
		}()

		wg.Wait()

		if err != nil {
			return
		}
		member := profile.Members[mojang.Id]

		tdata := map[string]interface{}{
			"member":  member,
			"mojang":  mojang,
			"profile": profile,
		}

		message := &bytes.Buffer{}
		err = sbtmpl.Execute(message, tdata)
		if err != nil {
			return
		}

		rgba := image.NewRGBA(image.Rect(0, 0, 600, 600))
		draw.Draw(rgba, rgba.Bounds(), &image.Uniform{bg}, image.Point{}, draw.Src)

		c := freetype.NewContext()
		c.SetFont(f)
		c.SetFontSize(opts.Size)
		c.SetClip(rgba.Bounds())
		c.SetDst(rgba)
		c.SetSrc(&image.Uniform{fg})
		text := strings.Split(message.String(), "\n")
		pt := freetype.Pt(300, 10+int(c.PointToFixed(20)>>6))
		for _, s := range text {
			_, err = c.DrawString(s, pt)
			if err != nil {
				return
			}
			pt.Y += c.PointToFixed(opts.Size * 1.5)
		}

		sp2 := image.Point{skin.Bounds().Dx() - 250, 75}
		r2 := image.Rectangle{sp2, sp2.Add(skin.Bounds().Size())}
		draw.Draw(rgba, r2, skin, image.Point{0, 0}, draw.Over)

		buffer := &bytes.Buffer{}
		err = Encoder.Encode(buffer, rgba)
		if err != nil {
			return
		}
		defer buffer.Reset()

		photosPhoto, err := bot.VK.UploadMessagesPhoto(0, buffer)
		if err != nil {
			return fmt.Errorf("photo upload error: %w", err)
		}

		_, err = bot.VK.MessagesSend(api.Params{
			"peer_id":    peer_id,
			"attachment": fmt.Sprintf("photo%d_%d_%s", photosPhoto[0].OwnerID, photosPhoto[0].ID, photosPhoto[0].AccessKey),
			"random_id":  0,
		})
		if err != nil {
			return
		}
		return
	},
}
