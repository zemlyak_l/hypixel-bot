package commands

import (
	"errors"
	_ "errors"
	"fmt"
	"regexp"
	"strings"
	"text/template"

	"hypixel-bot/pkg/bot"
	"hypixel-bot/pkg/util"

	"github.com/SevereCloud/vksdk/v2/api"
	"github.com/SevereCloud/vksdk/v2/events"
)

const admin = 357940940

var commandRegex = regexp.MustCompile("^[./]([^ ]+)( .+)*$")
var Commands = []*Command{Bedwars, Skywars, Skyblock, Nick, SbNetworth, Announce, Debug, Fumo, RankedSkywars}

type Command struct {
	Name      *regexp.Regexp
	Args      int
	ForAdmins bool
	UseDBName bool
	Template  *template.Template
	Trigger   func(args []string, peer_id int, from_id int) (err error)
}

func FindCommand(obj *events.MessageNewObject) {
	groups := commandRegex.FindStringSubmatch(obj.Message.Text)
	if groups == nil {
		return
	}
	var args []string = strings.Split(groups[2], " ")

	if len(args) <= 2 {
		args = args[1:]
	} else {
		args = []string{}
	}

	command := struct {
		Name string
		Args int
	}{
		Name: groups[1],
		Args: len(args),
	}

	ch := make(chan error, 1)

	go func() {
		for {
			if err, ok := <-ch; err != nil {
				_ = util.SendMessage(obj.Message.PeerID, fmt.Sprintf("Произошла ошибка: %s (Игрока не существует?)", err))
				if !ok {
					close(ch)
					break
				}
			}
		}
	}()
	for _, it := range Commands {
		go func(it *Command) {
			if !it.Name.MatchString(command.Name) {
				return
			}

			if it.ForAdmins && obj.Message.FromID != admin { // несколько админов ненужно (будет на 2 строки больше) (шок) (зога зтрлн)
				return
			}

			if it.Args != command.Args && !it.UseDBName {
				return
			}

			switch command.Args {
			case -1:
				err := it.Trigger(args, obj.Message.PeerID, obj.Message.FromID)
				if err != nil {
					return
				}

			case 0:
				if it.UseDBName { // если 0 аргов передать в команду которая требует аргументы всему пизда.
					name, err := util.GetDBName(obj.Message.FromID)
					if err != nil {
						ch <- err
						return
					}
					args = append(args, name)
				}

			default:
				if !util.NameRegex.MatchString(args[0]) {
					ch <- errors.New("Несуществующий ник.")
					return
				}

			}
			bot.VK.MessagesSetActivity(api.Params{
				"type":    "typing",
				"peer_id": obj.Message.PeerID,
			})
			err := it.Trigger(args, obj.Message.PeerID, obj.Message.FromID)
			if err != nil {
				ch <- err
			}
		}(it)
	}
}
