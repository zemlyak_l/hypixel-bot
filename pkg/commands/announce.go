package commands

import (
	"hypixel-bot/pkg/bot"
	"regexp"
	"strconv"
	"strings"

	"github.com/SevereCloud/vksdk/v2/api"
)

var Announce = &Command{
	Name:      regexp.MustCompile("^(announce|анонс)$"),
	Args:      -1,
	ForAdmins: true,
	UseDBName: false,
	Trigger: func(args []string, peer_id int, from_id int) (err error) {
		chats := []string{}
		conv, err := bot.VK.MessagesGetConversations(api.Params{})
		if err != nil {
			return
		}

		for _, x := range conv.Items {
			if x.Conversation.CanWrite.Allowed && x.Conversation.Peer.Type == "chat" {
				chats = append(chats, strconv.Itoa(x.Conversation.Peer.ID))
			}
		}

		for i := 0; i < len(chats); i += 100 {
			batch := chats[i:min(i+100, len(chats))]
			_, err = bot.VK.MessagesSendPeerIDs(api.Params{
				"message": strings.Join(args, " "),

				"peer_ids":  strings.Join(batch, ","),
				"random_id": 0,
			})
		}

		return
	},
}

func min(a, b int) int {
	if a <= b {
		return a
	}
	return b
}
