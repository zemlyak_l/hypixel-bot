package commands

import (
	"encoding/json"
	"errors"
	"fmt"
	"hypixel-bot/pkg/util"
	"regexp"
	"strings"

	"github.com/valyala/fasthttp"
)

func appendFirstFive(to *[]string, what *[]util.TopItems) {
	capacity := 5
	if len(*what) <= 5 {
		capacity = len(*what)
	}
	for _, x := range (*what)[0:capacity] {
		*to = append(*to, fmt.Sprintf("%s → %s", x.Name, util.FormatInt(x.Price)))
	}
}

var SbNetworth = &Command{
	Name:      regexp.MustCompile("^(nw|networth|нетворх|нв)$"),
	Args:      1,
	ForAdmins: false,
	UseDBName: true,
	Trigger: func(args []string, peer_id int, from_id int) (err error) {
		mojang, err := util.GetUUID(args[0])
		if err != nil {
			return
		}
		res, err := util.GetHypixelApi("skyblock/profiles", "&uuid="+mojang.Id)
		if err != nil {
			return
		}

		profile := &util.HypixelSkyblockReturn{}

		err = json.Unmarshal([]byte(res), &profile)
		if err != nil {
			return
		}

		var comparison int64
		var activeProfile util.SkyblockProfile
		for _, x := range profile.Profiles {
			if x.Members[mojang.Id].LastSave > comparison {
				comparison = x.Members[mojang.Id].LastSave
				activeProfile = x
			}
		}
		if entry, ok := activeProfile.Members[mojang.Id]; ok {
			entry.Banking = activeProfile.Banking
			activeProfile.Members[mojang.Id] = entry
		}

		data, err := json.Marshal(activeProfile.Members[mojang.Id])
		if err != nil {
			return
		}
		req := fasthttp.AcquireRequest()
		resp := fasthttp.AcquireResponse()
		defer fasthttp.ReleaseRequest(req)
		defer fasthttp.ReleaseResponse(resp)

		req.Header.SetMethod("POST")
		req.Header.SetContentType("application/json")
		req.SetBodyString("{\"data\":" + string(data) + "}")
		req.SetRequestURI("https://skyblock.acebot.xyz/api/networth/categories")
		if err := util.Client.Do(req, resp); err != nil {
			return err
		}

		body := resp.Body()

		networth := &util.NetworthReturn{}
		err = json.Unmarshal(body, &networth)
		if err != nil {
			return
		}

		if networth.Status != 200 {
			return errors.New("Закрыт доступ к инвентарю. https://sky.shiiyu.moe/resources/video/enable-api.mp4")
		}

		message := []string{}

		// говнокод урааа
		message = append(message, fmt.Sprintf("🍀 Total → %s", util.FormatInt(networth.Data.Networth+networth.Data.Bank+networth.Data.Purse+networth.Data.Sacks)))
		message = append(message, fmt.Sprintf("💰 Purse → %s", util.FormatInt(networth.Data.Purse)))
		message = append(message, fmt.Sprintf("💵 Bank → %s", util.FormatInt(networth.Data.Bank)))

		message = append(message, fmt.Sprintf("\n🗄 Storage → %s", util.FormatInt(networth.Data.Categories.Storage.Total)))
		appendFirstFive(&message, &networth.Data.Categories.Storage.TopItems)

		message = append(message, fmt.Sprintf("\n📦 Инвентарь → %s", util.FormatInt(networth.Data.Categories.Inventory.Total)))
		appendFirstFive(&message, &networth.Data.Categories.Inventory.TopItems)

		message = append(message, fmt.Sprintf("\n🛡️ Броня → %s", util.FormatInt(networth.Data.Categories.Armor.Total)))
		appendFirstFive(&message, &networth.Data.Categories.Armor.TopItems)

		message = append(message, fmt.Sprintf("\n👚 Wardrobe → %s", util.FormatInt(networth.Data.Categories.WardrobeInventory.Total)))
		appendFirstFive(&message, &networth.Data.Categories.WardrobeInventory.TopItems)

		message = append(message, fmt.Sprintf("\n🦆 Питомцы → %s", util.FormatInt(networth.Data.Categories.Pets.Total)))
		appendFirstFive(&message, &networth.Data.Categories.Pets.TopItems)

		message = append(message, fmt.Sprintf("\n🔮 Талисманы → %s", util.FormatInt(networth.Data.Categories.Talismans.Total)))
		appendFirstFive(&message, &networth.Data.Categories.Talismans.TopItems)

		err = util.SendMessage(peer_id, strings.Join(message, "\n"))

		return
	},
}
