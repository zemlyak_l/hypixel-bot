package poll

import (
	"context"
	"hypixel-bot/pkg/commands"
	"log"
	"net/http"
	"os"

	"github.com/SevereCloud/vksdk/v2/callback"
	"github.com/SevereCloud/vksdk/v2/events"
)

func StartCallback() {
	log.Println("Started http server on port 8080")

	cb := callback.NewCallback()

	cb.ConfirmationKey = os.Getenv("CONFIRMATION_KEY")
	cb.SecretKey = os.Getenv("SECRET_KEY")

	cb.MessageNew(func(ctx context.Context, obj events.MessageNewObject) {
		if obj.Message.PeerID != 2000000025 {
			log.Printf("%d: %s from %d", obj.Message.PeerID, obj.Message.Text, obj.Message.FromID)
			go commands.FindCommand(&obj)
		}
	})

	http.HandleFunc("/callback", cb.HandleFunc)

	if err := http.ListenAndServe(":8080", nil); err != nil {
		log.Fatal(err)
	}

}
