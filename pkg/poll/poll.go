package poll

import "os"

func Poll() {
	if os.Getenv("MODE") == "prod" {
		StartCallback()
	} else {
		StartLongpoll()
	}
}
