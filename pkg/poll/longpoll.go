package poll

import (
	"context"
	"hypixel-bot/pkg/bot"
	"hypixel-bot/pkg/commands"
	"log"

	"github.com/SevereCloud/vksdk/v2/events"
	"github.com/SevereCloud/vksdk/v2/longpoll-bot"
)

func StartLongpoll() {
	group, err := bot.VK.GroupsGetByID(nil)
	if err != nil {
		log.Fatal(err)
	}

	lp, err := longpoll.NewLongPoll(bot.VK, group[0].ID)
	if err != nil {
		log.Fatal(err)
	}
	lp.MessageNew(func(_ context.Context, obj events.MessageNewObject) {
		log.Printf("%d: %s from %d", obj.Message.PeerID, obj.Message.Text, obj.Message.FromID)
		commands.FindCommand(&obj)
	})

	log.Println("Start Long Poll")
	if err := lp.Run(); err != nil {
		log.Fatal(err)
	}
}
