package bot

import (
	"database/sql"
	"log"

	"github.com/SevereCloud/vksdk/v2/api"
	_ "modernc.org/sqlite"
)

var VK *api.VK
var DB *sql.DB
var HypixelKey string

func New(token string, key string) {
	var err error
	VK = api.NewVK(token)
	HypixelKey = key
	DB, err = sql.Open("sqlite", "db.sql")
	if err != nil {
		log.Fatal(err)
	}
}
